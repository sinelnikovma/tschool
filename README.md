# tschool

Default using database is PostgreSQL. For changing used database should correct *root-context.xml* file and *security-config.xml*.

To need changed param *dataSource* at beans *entityManagerFactory* and *transactionManager* in *root-context.xml* and *jdbcGroupsImpl* in *security-config.xml*.

Values for datasource:
+ *dataSourceH2* is a datasource for H2 database.
+ *dataSourcePG*  is a datasource for PostgreSQL database.
+ *dataSourceMySQL* is a datasource for MySQL database.

To fill the tables with data should run command from file *data.sql*.

Logins:
+ admin - admin
+ doctor1 - doctor1
+ doctor2 - doctor2
+ nurse1 - nurse1
+ nurse2 - nurse2  