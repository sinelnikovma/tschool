package com.tsystems.rehabilitationcenter.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.rehabilitationcenter.dao.TreatmentEventDao;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDaoImpl;
import com.tsystems.rehabilitationcenter.model.TreatmentEvent;
import com.tsystems.rehabilitationcenter.view.TreatmentEventDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

import junit.framework.Assert;

public class TreatmentEventServiceImplTest {

	private TreatmentEventDao treatmentEventDao;
	private TreatmentEventService service;

	private List<TreatmentEvent> list = new ArrayList<>();
	private List<TreatmentEventDto> dtoList = new ArrayList<>();
	private List<TreatmentEventDto> notEqualList = new ArrayList<>();

	private TreatmentEvent etalone;
	private TreatmentEventDto etaloneDto;
	private TreatmentEventObjectFilter filter;

	@Before
	public void setUp() throws Exception {

		treatmentEventDao = mock(TreatmentEventDaoImpl.class);
		service = new TreatmentEventServiceImpl(treatmentEventDao);

		filter = mock(TreatmentEventObjectFilter.class);
		LocalDateTime dateTime = LocalDateTime.of(LocalDate.now(), LocalTime.of(0, 0));
		TreatmentEvent event = new TreatmentEvent("001", null, dateTime, "", "", null, "");
		event.setId(1L);
		list.add(event);

		event = new TreatmentEvent("002", null, dateTime, "", "", null, "");
		event.setId(2L);
		etalone = event;
		list.add(event);

		event = new TreatmentEvent("003", null, dateTime, "", "", null, "");
		event.setId(2L);
		list.add(event);

		TreatmentEventDto dtoEvent = new TreatmentEventDto();
		dtoEvent.setId(1L);
		dtoEvent.setCancelReason("");
		dtoEvent.setDateOfEvent(dateTime);
		dtoEvent.setStatusOfEvent("");
		dtoEvent.setDose("");
		dtoList.add(dtoEvent);
		notEqualList.add(dtoEvent);

		dtoEvent = new TreatmentEventDto();
		dtoEvent.setId(2L);
		dtoEvent.setCancelReason("");
		dtoEvent.setDateOfEvent(dateTime);
		dtoEvent.setStatusOfEvent("");
		dtoEvent.setDose("");
		etaloneDto = dtoEvent;
		dtoList.add(dtoEvent);
		notEqualList.add(dtoEvent);

		dtoEvent = new TreatmentEventDto();
		dtoEvent.setId(3L);
		dtoEvent.setCancelReason("");
		dtoEvent.setDateOfEvent(dateTime);
		dtoEvent.setStatusOfEvent("");
		dtoEvent.setDose("");
		dtoList.add(dtoEvent);
		notEqualList.add(dtoEvent);

		dtoEvent = new TreatmentEventDto();
		dtoEvent.setId(4L);
		dtoEvent.setCancelReason("");
		dtoEvent.setDateOfEvent(dateTime);
		dtoEvent.setStatusOfEvent("");
		dtoEvent.setDose("");
		notEqualList.add(dtoEvent);
	}

	@Test
	public final void testGetTreatmentEventByFilter1() {
		when(treatmentEventDao.getTreatmentEventByFilter(filter)).thenReturn(list);

		List<TreatmentEventDto> treatmentEventByFilter = service.getTreatmentEventByFilter(filter);

		Assert.assertTrue(treatmentEventByFilter.containsAll(dtoList));
	}

	@Test
	public final void testGetTreatmentEventByFilter2() {
		when(treatmentEventDao.getTreatmentEventByFilter(filter)).thenReturn(list);

		List<TreatmentEventDto> treatmentEventByFilter = service.getTreatmentEventByFilter(filter);

		Assert.assertTrue(treatmentEventByFilter.containsAll(notEqualList));
	}

	@Test
	public final void testGetAllTreatmentEvent1() {
		when(treatmentEventDao.getAllTreatmentEvent()).thenReturn(list);

		List<TreatmentEventDto> allTreatmentEvent = service.getAllTreatmentEvent();

		Assert.assertTrue(allTreatmentEvent.containsAll(dtoList));
	}

	@Test
	public final void testGetAllTreatmentEvent2() {
		when(treatmentEventDao.getAllTreatmentEvent()).thenReturn(list);

		List<TreatmentEventDto> allTreatmentEvent = service.getAllTreatmentEvent();

		Assert.assertTrue(allTreatmentEvent.containsAll(notEqualList));
	}

	@Test
	public final void testChangeStatus() {

		service.changeStatus(etaloneDto);

		Mockito.verify(treatmentEventDao, times(0)).changeStatus(etalone);
	}

	@Test
	public final void testGetTreatmentEventById() {
		when(treatmentEventDao.getTreatmentEventById(2L)).thenReturn(etalone);

		TreatmentEventDto treatmentEventById = service.getTreatmentEventById(2L);

		Assert.assertEquals(treatmentEventById, etaloneDto);
	}

}
