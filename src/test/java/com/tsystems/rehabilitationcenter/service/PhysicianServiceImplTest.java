package com.tsystems.rehabilitationcenter.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.tsystems.rehabilitationcenter.dao.PhysicianDao;
import com.tsystems.rehabilitationcenter.dao.PhysicianDaoImpl;
import com.tsystems.rehabilitationcenter.model.Physician;
import com.tsystems.rehabilitationcenter.view.PhysicianDto;

import junit.framework.Assert;

public class PhysicianServiceImplTest {

	private PhysicianDao physicianDao;
	private Set<Physician> set = new HashSet<>();
	private List<PhysicianDto> list = new ArrayList<>();
	private List<PhysicianDto> notEqualList = new ArrayList<>();
	private Physician physician;
	private PhysicianDto physicianDto;
	private PhysicianService service;

	@Before
	public void setUp() throws Exception {
		physicianDao = mock(PhysicianDaoImpl.class);

		physician = new Physician("doctor1", "doctor1_last", "001", null, null, true);
		set.add(physician);

		physician = new Physician("doctor2", "doctor2_last", "002", null, null, true);
		set.add(physician);

		physician = new Physician("doctor3", "doctor3_last", "003", null, null, true);
		set.add(physician);

		physicianDto = new PhysicianDto();
		physicianDto.setPhysicianFirstName("doctor1");
		physicianDto.setPhysicianLastName("doctor1_last");
		physicianDto.setPhysicianCode("001");
		list.add(physicianDto);
		notEqualList.add(physicianDto);

		physicianDto = new PhysicianDto();
		physicianDto.setPhysicianFirstName("doctor2");
		physicianDto.setPhysicianLastName("doctor2_last");
		physicianDto.setPhysicianCode("002");
		list.add(physicianDto);
		notEqualList.add(physicianDto);

		physicianDto = new PhysicianDto();
		physicianDto.setPhysicianFirstName("doctor3");
		physicianDto.setPhysicianLastName("doctor3_last");
		physicianDto.setPhysicianCode("003");
		list.add(physicianDto);
		notEqualList.add(physicianDto);

		physicianDto = new PhysicianDto();
		physicianDto.setPhysicianFirstName("doctor4");
		physicianDto.setPhysicianLastName("doctor4_last");
		physicianDto.setPhysicianCode("004");
		notEqualList.add(physicianDto);

		service = new PhysicianServiceImpl(physicianDao);
	}

	@Test
	public final void testGetPhisicianAll1() {
		when(physicianDao.getAllPhysician()).thenReturn(set);

		List<PhysicianDto> phisicianAll = service.getPhisicianAll();

		Assert.assertTrue(list.containsAll(phisicianAll));
	}

	@Test
	public final void testGetPhisicianAll2() {
		when(physicianDao.getAllPhysician()).thenReturn(set);

		List<PhysicianDto> phisicianAll = service.getPhisicianAll();

		Assert.assertFalse(phisicianAll.containsAll(notEqualList));
	}
}
