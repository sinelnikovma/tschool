package com.tsystems.rehabilitationcenter.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.rehabilitationcenter.dao.DrugDao;
import com.tsystems.rehabilitationcenter.dao.DrugDaoImpl;
import com.tsystems.rehabilitationcenter.dao.PatientDao;
import com.tsystems.rehabilitationcenter.dao.PatientDaoImpl;
import com.tsystems.rehabilitationcenter.dao.PrescriptionDao;
import com.tsystems.rehabilitationcenter.dao.PrescriptionDaoImpl;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDao;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDaoImpl;
import com.tsystems.rehabilitationcenter.model.Drug;
import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Prescription;
import com.tsystems.rehabilitationcenter.view.DrugDto;
import com.tsystems.rehabilitationcenter.view.PatientDto;
import com.tsystems.rehabilitationcenter.view.PrescriptionDto;

import junit.framework.Assert;

public class PrescriptionServiceImplTest {

	private PrescriptionDao prescriptionDao;
	private PatientDao patientDao;
	private DrugDao drugDao;
	private TreatmentEventDao treatmentEventDao;
	private PrescriptionService service;

	private Set<Prescription> set = new HashSet<>();
	private Set<PrescriptionDto> setDto = new HashSet<>();
	private Set<PrescriptionDto> notEqualSet = new HashSet<>();

	private Prescription prescription;
	private Prescription etalone;
	private PrescriptionDto prescriptionDto;
	private PrescriptionDto etaloneDto;
	private PrescriptionDto notEqualDto;
	private Patient etalonePatient;
	private PatientDto patientDto;
	private Drug etaloneDrug;
	private DrugDto etaloneDrugDto;

	@Before
	public void setUp() throws Exception {
		prescriptionDao = mock(PrescriptionDaoImpl.class);
		patientDao = mock(PatientDaoImpl.class);
		drugDao = mock(DrugDaoImpl.class);
		treatmentEventDao = mock(TreatmentEventDaoImpl.class);

		etaloneDrug = new Drug();
		etaloneDrug.setId(1L);

		etaloneDrugDto = new DrugDto();
		etaloneDrugDto.setId(1L);

		etalonePatient = new Patient("patient2", "patientLast2", "", "002", null, null, null, "", true);
		etalonePatient.setId(2L);

		service = new PrescriptionServiceImpl(prescriptionDao, patientDao, drugDao, treatmentEventDao, null);

		prescription = new Prescription("001", etalonePatient, null, LocalDate.now(), LocalDate.now(), new HashSet<>(),
				true, true, true, true, true, true, true, false, "", true);
		prescription.setId(1L);
		set.add(prescription);

		prescription = new Prescription("002", etalonePatient, null, LocalDate.now(), LocalDate.now(), new HashSet<>(),
				true, true, true, true, true, true, true, true, "", true);
		prescription.setId(2L);
		etalone = prescription;
		set.add(prescription);

		prescription = new Prescription("003", etalonePatient, null, LocalDate.now(), LocalDate.now(), new HashSet<>(),
				true, true, true, true, true, true, true, true, "", true);
		prescription.setId(3L);
		set.add(prescription);

		prescriptionDto = new PrescriptionDto();
		prescriptionDto.setPrescriptionNumber("001");
		prescriptionDto.setId(1L);
		prescriptionDto.setBeginDate(LocalDate.now().toString());
		prescriptionDto.setEndDate(LocalDate.now().toString());
		prescriptionDto.setDrugType(etaloneDrugDto);
		patientDto = new PatientDto();
		patientDto.setId(1L);
		prescriptionDto.setPatient(patientDto);
		setDto.add(prescriptionDto);
		notEqualSet.add(prescriptionDto);

		prescriptionDto = new PrescriptionDto();
		prescriptionDto.setPrescriptionNumber("002");
		prescriptionDto.setId(2L);
		prescriptionDto.setBeginDate(LocalDate.now().toString());
		prescriptionDto.setEndDate(LocalDate.now().toString());
		prescriptionDto.setDrugType(etaloneDrugDto);
		patientDto = new PatientDto();
		patientDto.setId(2L);
		prescriptionDto.setPatient(patientDto);
		etaloneDto = prescriptionDto;
		setDto.add(prescriptionDto);
		notEqualSet.add(prescriptionDto);

		prescriptionDto = new PrescriptionDto();
		prescriptionDto.setPrescriptionNumber("003");
		prescriptionDto.setId(3L);
		prescriptionDto.setBeginDate(LocalDate.now().toString());
		prescriptionDto.setEndDate(LocalDate.now().toString());
		prescriptionDto.setDrugType(etaloneDrugDto);
		patientDto = new PatientDto();
		patientDto.setId(3L);
		prescriptionDto.setPatient(patientDto);
		setDto.add(prescriptionDto);
		notEqualSet.add(prescriptionDto);

		prescriptionDto = new PrescriptionDto();
		prescriptionDto.setPrescriptionNumber("004");
		prescriptionDto.setId(4L);
		prescriptionDto.setBeginDate(LocalDate.now().toString());
		prescriptionDto.setEndDate(LocalDate.now().toString());
		prescriptionDto.setDrugType(etaloneDrugDto);
		patientDto = new PatientDto();
		patientDto.setId(4L);
		prescriptionDto.setPatient(patientDto);
		notEqualDto = prescriptionDto;
		notEqualSet.add(prescriptionDto);
	}

	@Test
	public final void testGetAllPrescriptions1() {
		when(prescriptionDao.getAllPrescriptions()).thenReturn(set);

		Set<PrescriptionDto> allPrescriptions = service.getAllPrescriptions();

		Assert.assertTrue(allPrescriptions.containsAll(setDto));
	}

	@Test
	public final void testGetAllPrescriptions2() {
		when(prescriptionDao.getAllPrescriptions()).thenReturn(set);

		Set<PrescriptionDto> allPrescriptions = service.getAllPrescriptions();

		Assert.assertFalse(allPrescriptions.containsAll(notEqualSet));
	}

	@Test
	public final void testGetPrescriptionByNumber1() {
		when(prescriptionDao.getPrescriptionByNumber("002")).thenReturn(etalone);

		PrescriptionDto prescriptionByNumber = service.getPrescriptionByNumber("002");

		Assert.assertEquals(prescriptionByNumber, etaloneDto);
	}

	@Test
	public final void testGetPrescriptionByNumber2() {
		when(prescriptionDao.getPrescriptionByNumber("002")).thenReturn(etalone);

		PrescriptionDto prescriptionByNumber = service.getPrescriptionByNumber("002");

		Assert.assertFalse(prescriptionByNumber.equals(notEqualDto));
	}

	@Test
	public final void testGetPrescriptionByPatientId1() {
		when(prescriptionDao.getPrescriptionsByPatient(2L)).thenReturn(set);

		Set<PrescriptionDto> prescriptionByPatientId = service.getPrescriptionByPatientId(2L);

		Assert.assertTrue(prescriptionByPatientId.containsAll(setDto));
	}

	@Test
	public final void testGetPrescriptionByPatientId2() {
		when(prescriptionDao.getPrescriptionsByPatient(2L)).thenReturn(set);

		Set<PrescriptionDto> prescriptionByPatientId = service.getPrescriptionByPatientId(2L);

		Assert.assertFalse(prescriptionByPatientId.containsAll(notEqualSet));
	}

	@Test
	public final void testGetPrescriptionById1() {
		when(prescriptionDao.getPrescriptionById(3L)).thenReturn(etalone);

		PrescriptionDto prescriptionById = service.getPrescriptionById(3L);

		Assert.assertEquals(prescriptionById, etaloneDto);
	}

	@Test
	public final void testGetPrescriptionById2() {
		when(prescriptionDao.getPrescriptionById(3L)).thenReturn(etalone);

		PrescriptionDto prescriptionById = service.getPrescriptionById(3L);

		Assert.assertFalse(prescriptionById.equals(notEqualDto));
	}

	@Test
	public final void testCreatePrescription1() {
		when(prescriptionDao.getPrescriptionByNumber("002")).thenReturn(etalone);
		when(patientDao.getPatientById(2L)).thenReturn(etalonePatient);
		when(prescriptionDao.getPrescriptionById(2L)).thenReturn(etalone);
		when(drugDao.getDrugById(1L)).thenReturn(etaloneDrug);

		service.createPrescription(etaloneDto);

		Mockito.verify(prescriptionDao, times(1)).updatePrescription(etalone);
	}

	@Test
	public final void testCreatePrescription2() {
		when(prescriptionDao.getPrescriptionByNumber("002")).thenReturn(etalone);
		when(patientDao.getPatientById(2L)).thenReturn(etalonePatient);
		when(prescriptionDao.getPrescriptionById(2L)).thenReturn(etalone);
		when(drugDao.getDrugById(1L)).thenReturn(etaloneDrug);

		service.createPrescription(etaloneDto);

		Mockito.verify(prescriptionDao, times(0)).createPrescrition(etalone);
	}

	@Test
	public final void testDeletePrescription1() {

		service.deletePrescription(etaloneDto);

		Mockito.verify(treatmentEventDao, times(1)).deleteTreatmentEventByPrescriptionNumber("002");
	}

	@Test
	public final void testDeletePrescription2() {

		service.deletePrescription(etaloneDto);

		Mockito.verify(prescriptionDao, times(1)).deletePrescription(etalone);
	}
}
