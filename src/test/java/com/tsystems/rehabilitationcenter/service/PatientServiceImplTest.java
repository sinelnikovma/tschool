package com.tsystems.rehabilitationcenter.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.when;

import java.security.Principal;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;
import org.mockito.Mockito;

import com.tsystems.rehabilitationcenter.dao.PatientDao;
import com.tsystems.rehabilitationcenter.dao.PatientDaoImpl;
import com.tsystems.rehabilitationcenter.dao.PhysicianDao;
import com.tsystems.rehabilitationcenter.dao.PhysicianDaoImpl;
import com.tsystems.rehabilitationcenter.dao.PrescriptionDao;
import com.tsystems.rehabilitationcenter.dao.PrescriptionDaoImpl;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDao;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDaoImpl;
import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Physician;
import com.tsystems.rehabilitationcenter.view.PatientDto;

import junit.framework.Assert;

public class PatientServiceImplTest {

	private PatientDao patientDao;
	private PhysicianDao physicianDao;
	private TreatmentEventDao treatmentEventDao;
	private PrescriptionDao prescriptionDao;
	private PatientService service;
	private Patient patient;
	private Patient etalonePatient;
	private PatientDto patientDto;
	private PatientDto etalonePatientDto;
	private Physician physician;
	private Principal principal;

	private List<PatientDto> list = new ArrayList<>();
	private Set<Patient> set = new HashSet<>();
	private List<PatientDto> notEqual = new ArrayList<>();

	@Before
	public void setUp() throws Exception {
		patientDao = mock(PatientDaoImpl.class);
		physicianDao = mock(PhysicianDaoImpl.class);
		treatmentEventDao = mock(TreatmentEventDaoImpl.class);
		prescriptionDao = mock(PrescriptionDaoImpl.class);
		physician = mock(Physician.class);
		principal = mock(Principal.class);
		service = new PatientServiceImpl(patientDao, physicianDao, treatmentEventDao, prescriptionDao, null);

		patient = new Patient("Иван", "Иванов", "bad", "111", null, null, null, "", true);
		patient.setId(1L);
		set.add(patient);

		patient = new Patient("Петя", "Петров", "bad", "222", null, null, null, "", true);
		patient.setId(2L);
		set.add(patient);
		etalonePatient = patient;

		patient = new Patient("Сидр", "Сидоров", "bad", "333", null, null, null, "", true);
		patient.setId(3L);
		set.add(patient);
		notEqual.add(patientDto);

		patientDto = new PatientDto();
		patientDto.setId(1L);
		patientDto.setPatientFirstName("Иван");
		patientDto.setPatientLastName("Иванов");
		patientDto.setDiagnosis("bad");
		patientDto.setDoctor(null);
		patientDto.setInsuranceNumber("111");

		list.add(patientDto);
		notEqual.add(patientDto);

		patientDto = new PatientDto();
		patientDto.setId(1L);
		patientDto.setPatientFirstName("Петя");
		patientDto.setPatientLastName("Петров");
		patientDto.setDiagnosis("bad");
		patientDto.setDoctor(null);
		patientDto.setInsuranceNumber("222");

		list.add(patientDto);
		notEqual.add(patientDto);
		etalonePatientDto = patientDto;

		patientDto = new PatientDto();
		patientDto.setId(1L);
		patientDto.setPatientFirstName("Сидр");
		patientDto.setPatientLastName("Сидоров");
		patientDto.setDiagnosis("bad");
		patientDto.setDoctor(null);
		patientDto.setInsuranceNumber("333");

		list.add(patientDto);

		patientDto = new PatientDto();
		patientDto.setId(1L);
		patientDto.setPatientFirstName("stub");
		patientDto.setPatientLastName("stub");
		patientDto.setDiagnosis("bad");
		patientDto.setDoctor(null);
		patientDto.setInsuranceNumber("444");

		notEqual.add(patientDto);
	}

	@Test
	public final void testGetAllPatient1() {

		when(patientDao.getAllPatients()).thenReturn(set);

		List<PatientDto> allPatient = service.getAllPatient();

		Assert.assertTrue(list.containsAll(allPatient));
	}

	@Test
	public final void testGetAllPatient2() {

		when(patientDao.getAllPatients()).thenReturn(set);

		List<PatientDto> allPatient = service.getAllPatient();

		Assert.assertFalse(notEqual.containsAll(allPatient));
	}

	@Test
	public final void testGetPatientById1() {
		when(patientDao.getPatientById(2L)).thenReturn(etalonePatient);

		PatientDto patientById = service.getPatientById(2L);

		Assert.assertEquals(patientById, etalonePatientDto);
	}

	@Test
	public final void testGetPatientById2() {
		when(patientDao.getPatientById(2L)).thenReturn(etalonePatient);

		PatientDto patientById = service.getPatientById(2L);

		Assert.assertFalse(new PatientDto().equals(patientById));
	}

	@Test
	public final void testGetPatientsByPhysician() {
		when(physicianDao.getPhysicianBySecureId("")).thenReturn(physician);
		when(patientDao.getPatientsByPhysician(physician)).thenReturn(set);
		when(principal.getName()).thenReturn("");

		Mockito.verify(physicianDao, times(0)).getPhysicianBySecureId("");
	}

	@Test
	public final void testDischargePatient() {
		when(patientDao.getPatientById(1L)).thenReturn(patient);
		service.dischargePatient(1L);

		Mockito.verify(treatmentEventDao).deleteTreatmentEventByPatient(patient);
	}

}
