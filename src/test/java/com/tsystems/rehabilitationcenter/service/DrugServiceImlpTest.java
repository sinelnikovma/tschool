package com.tsystems.rehabilitationcenter.service;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

import com.tsystems.rehabilitationcenter.dao.DrugDao;
import com.tsystems.rehabilitationcenter.dao.DrugDaoImpl;
import com.tsystems.rehabilitationcenter.model.Drug;
import com.tsystems.rehabilitationcenter.view.DrugDto;

import junit.framework.Assert;

public class DrugServiceImlpTest {

	private Set<Drug> set = new HashSet<>();
	private List<DrugDto> dtos = new ArrayList<>();
	private List<DrugDto> nonEqual = new ArrayList<>();
	private DrugDao drugDao;
	private DrugService service;

	@Before
	public void setUp() throws Exception {
		drugDao = mock(DrugDaoImpl.class);
		service = new DrugServiceImlp(drugDao);

		for (int i = 0; i < 3; i++) {
			Drug drug = new Drug();
			drug.setId((long) i);
			drug.setDrugType("pill");
			drug.setActive(true);
			drug.setDrugName("pill " + i);

			set.add(drug);
		}

		for (int i = 0; i < 3; i++) {
			DrugDto dto = new DrugDto();
			dto.setId((long) i);
			dto.setActive(true);
			dto.setDrugType("pill");
			dto.setDrugName("pill " + i);

			dtos.add(dto);
		}

		nonEqual.addAll(dtos);

		DrugDto dto = new DrugDto();
		dto.setId((long) 4);
		dto.setActive(true);
		dto.setDrugType("pill");
		dto.setDrugName("pill 4");

		nonEqual.add(dto);
	}

	@Test
	public final void testGetAllDrug1() {
		when(drugDao.getAllDrugs()).thenReturn(set);

		List<DrugDto> allDrug = service.getAllDrug();

		Assert.assertTrue(allDrug.containsAll(dtos));
	}

	@Test
	public final void testGetAllDrug2() {
		when(drugDao.getAllDrugs()).thenReturn(set);

		List<DrugDto> allDrug = service.getAllDrug();

		Assert.assertFalse(allDrug.containsAll(nonEqual));
	}

}
