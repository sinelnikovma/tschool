CREATE TABLE users (
	username varchar(50) NOT NULL,
	enabled bool NOT NULL,
	"password" varchar(60) NOT NULL,
	CONSTRAINT users_pkey PRIMARY KEY (username)
);

CREATE TABLE groups (
	id int8 NOT NULL,
	group_name varchar(50) NOT NULL,
	CONSTRAINT groups_pkey PRIMARY KEY (id)
);

CREATE TABLE group_authorities (
	group_role_id int4 NOT NULL,
	authority varchar(50) NOT NULL,
	group_id int8 NOT NULL,
	CONSTRAINT group_authorities_pkey PRIMARY KEY (group_role_id),
	CONSTRAINT fk_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id)
);

CREATE TABLE group_members (
	id int8 NOT NULL,
	username varchar(50) NOT NULL,
	group_id int8 NOT NULL,
	CONSTRAINT group_members_pkey PRIMARY KEY (id),
	CONSTRAINT fk_group_id_id FOREIGN KEY (group_id) REFERENCES groups(id)
);

CREATE TABLE drug (
	id int8 NOT NULL,
	drug_name varchar(255) NULL,
	drug_type varchar(255) NULL,
	active bool NULL,
	"version" int4 NULL,
	CONSTRAINT drug_pkey PRIMARY KEY (id)
);

CREATE TABLE physician (
	id int8 NOT NULL,
	active bool NULL,
	physician_code varchar(255) NULL,
	physician_first_name varchar(255) NULL,
	physician_last_name varchar(255) NULL,
	"version" int4 NULL,
	user_id int8 NOT NULL,
	CONSTRAINT physician_pkey PRIMARY KEY (id),
	CONSTRAINT fk_group_member_id FOREIGN KEY (user_id) REFERENCES group_members(id)
);

CREATE TABLE patient (
	id int8 NOT NULL,
	diagnosis varchar(255) NULL,
	insurance_number varchar(255) NULL,
	active bool NULL,
	patient_first_name varchar(255) NULL,
	patient_last_name varchar(255) NULL,
	status varchar(255) NULL,
	"version" int4 NULL,
	doctor_id int8 NULL,
	CONSTRAINT patient_pkey PRIMARY KEY (id),
	CONSTRAINT fk_physician_id FOREIGN KEY (doctor_id) REFERENCES physician(id)
);

CREATE TABLE prescription (
	id int8 NOT NULL,
	begin_date date NULL,
	dose varchar(255) NULL,
	end_date date NULL,
	friday_check bool NULL,
	active bool NULL,
	monday_check bool NULL,
	prescription_number varchar(255) NOT NULL,
	saturday_check bool NULL,
	sunday_check bool NULL,
	thursday_check bool NULL,
	tuesday_check bool NULL,
	"version" int4 NULL,
	wednesday_check bool NULL,
	week_check bool NULL,
	drug_id int8 NULL,
	patient_id int8 NULL,
	CONSTRAINT prescription_pkey PRIMARY KEY (id),
	CONSTRAINT fk_drug_id FOREIGN KEY (drug_id) REFERENCES drug(id),
	CONSTRAINT fk_patient_id FOREIGN KEY (patient_id) REFERENCES patient(id)
);

CREATE TABLE prescription_body (
	id int8 NOT NULL,
	"day" varchar(255) NULL,
	prescription_number varchar(255) NULL,
	reception_time time NULL,
	"version" int4 NULL,
	prescription_id int8 NULL,
	CONSTRAINT prescription_body_pkey PRIMARY KEY (id),
	CONSTRAINT fk_prescription_id FOREIGN KEY (prescription_id) REFERENCES prescription(id)
);

CREATE TABLE persistent_logins (
	series varchar(64) NOT NULL,
	last_used timestamp NOT NULL,
	"token" varchar(64) NOT NULL,
	username varchar(64) NOT NULL,
	CONSTRAINT persistent_logins_pkey PRIMARY KEY (series)
);

CREATE TABLE treatment_event (
	id int8 NOT NULL,
	date_of_event timestamp NULL,
	status_of_event varchar(255) NULL,
	"version" int4 NULL,
	drug_id int8 NULL,
	patient_id int8 NULL,
	CONSTRAINT treatment_event_pkey PRIMARY KEY (id),
	CONSTRAINT fk_patient_id FOREIGN KEY (patient_id) REFERENCES patient(id),
	CONSTRAINT fk_drug_id FOREIGN KEY (drug_id) REFERENCES drug(id)
);