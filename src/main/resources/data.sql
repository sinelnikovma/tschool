INSERT INTO users (username, enabled, password) VALUES('admin', true, '$2a$10$lAihRrQWZ0xurUkR2kH5xe7MnutoRNm9mOyHV6RIKXD1ciCb4UN22');
INSERT INTO users (username, enabled, password) VALUES('doctor1', true, '$2a$10$CkPFj5iptaZ5ezpbS6OT9OH90OzXJt9rX0yvN5ev0CR9QfdwmT.4e');
INSERT INTO users (username, enabled, password) VALUES('doctor2', true, '$2a$10$ZLi8e.32smLUvqrhG7mja.WCJlVyARNEAHK3acZLs8QfUFhpfRkAa');
INSERT INTO users (username, enabled, password) VALUES('nurse1', true, '$2a$10$mXes2VlMZPlVp5TLpIv.u.rjTsoRJWZ.9VCW3TLxFjZ83c3vWDBPS');
INSERT INTO users (username, enabled, password) VALUES('nurse2', true, '$2a$10$p8/pa33r5Dk4Mpiq0f5dz.BjNM2B4sR2TjYUBiLFH58PCeuqIRKtS');


INSERT INTO groups (id, group_name) VALUES(1, 'admin');
INSERT INTO groups (id, group_name) VALUES(2, 'doctor');
INSERT INTO groups (id, group_name) VALUES(3, 'nurse');


INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(1, 'ROLE_CREATE_PATIENT', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(2, 'ROLE_DISCHARGE_PATIENT', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(3, 'ROLE_ASSIGN_PRESCRIPTION', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(4, 'ROLE_CHANGE_PRESCRIPTION', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(5, 'ROLE_CANCEL_PRESCRIPTION', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(6, 'ROLE_VIEW_EVENT', 3);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(7, 'ROLE_CHANGE_EVENT_STATUS', 3);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(8, 'ROLE_ROOT', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(9, 'ROLE_ROOT', 3);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(10, 'ROLE_DOCTOR', 2);
INSERT INTO group_authorities (group_role_id, authority, group_id) VALUES(11, 'ROLE_NURSE', 3);


INSERT INTO group_members (id, username, group_id) VALUES(1, 'doctor1', 2);
INSERT INTO group_members (id, username, group_id) VALUES(2, 'doctor2', 2);
INSERT INTO group_members (id, username, group_id) VALUES(3, 'nurse1', 3);
INSERT INTO group_members (id, username, group_id) VALUES(4, 'nurse2', 3);


INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (1, 0, 'water', 'Оросительная клизма', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (2, 0, 'potion1', 'Витаминный напиток', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (3, 0, 'potion2', 'Настойка полыни', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (4, 0, 'pill1', 'Пенталгин', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (5, 0, 'pill2', 'Анальгин', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (6, 0, 'pill3', 'Валидол', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (7, 0, 'water', 'Душ Шарко', true);
INSERT INTO drug (id, version, drug_type, drug_name, active) VALUES (8, 0, 'electro', 'Цветотерапия', true);


INSERT INTO physician (id, version, physician_first_name, physician_last_name, physician_code, user_id, active) VALUES (1, 0, 'Александр', 'Лютый', '001', 1, true);
INSERT INTO physician (id, version, physician_first_name, physician_last_name, physician_code, user_id, active) VALUES (2, 0, 'Ахмет', 'Сабдулаев', '001', 2, true);
INSERT INTO physician (id, version, physician_first_name, physician_last_name, physician_code, user_id, active) VALUES (3, 0, 'Ольга', 'Рукосуева', '002', 3, true);
INSERT INTO physician (id, version, physician_first_name, physician_last_name, physician_code, user_id, active) VALUES (4, 0, 'Валентина', 'Могила', '002', 4, true);


INSERT INTO patient (id, version, patient_first_name, patient_last_name, diagnosis, insurance_number, doctor_id, status, active) VALUES (1, 0, 'Александр', 'Иванов', 'плохой', '100', 1, 'onTreatment', true);
INSERT INTO patient (id, version, patient_first_name, patient_last_name, diagnosis, insurance_number, doctor_id, status, active) VALUES (2, 0, 'Валентина', 'Нежданова', 'плохой', '200', 2, 'onTreatment', true);
INSERT INTO patient (id, version, patient_first_name, patient_last_name, diagnosis, insurance_number, doctor_id, status, active) VALUES (3, 0, 'Николай', 'Анисимов', 'плохой', '300', 2, 'writtenOut', true);
INSERT INTO patient (id, version, patient_first_name, patient_last_name, diagnosis, insurance_number, doctor_id, status, active) VALUES (4, 0, 'Светлана', 'Тихонова', 'плохой', '400', 1, 'onTreatment', true);
INSERT INTO patient (id, version, patient_first_name, patient_last_name, diagnosis, insurance_number, doctor_id, status, active) VALUES (5, 0, 'Алексей', 'Ерёмин', 'плохой', '500', 2, 'onTreatment', true);
INSERT INTO patient (id, version, patient_first_name, patient_last_name, diagnosis, insurance_number, doctor_id, status, active) VALUES (6, 0, 'Матвей', 'Глоткин', 'плохой', '600', 1, 'onTreatment', true);


INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(1, '2019-04-15', '', '2019-04-25', false, true, true, '0001', false, false, false, false, 0, false, false, 1, 1);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(2, '2019-04-15', '', '2019-04-25', false, true, false, '0002', false, false, false, true, 0, true, false, 2, 1);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(3, '2019-04-15', '', '2019-04-25', false, true, false, '0003', false, false, false, true, 0, false, false, 2, 1);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(4, '2019-04-15', '25мкг/125мкг', '2019-04-25', false, true, false, '0004', false, false, true, false, 0, false, false, 6, 3);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(5, '2019-04-15', '', '2019-04-25', false, true, false, '0005', false, false, false, false, 0, false, true, 3, 3);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(6, '2019-04-15', '100мкг', '2019-04-25', true, true, false, '0006', false, false, false, false, 0, false, false, 6, 4);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(7, '2019-04-15', '', '2019-04-25', false, true, true, '0007', false, false, false, false, 0, false, false, 1, 6);
INSERT INTO prescription (id, begin_date, dose, end_date, friday_check, active, monday_check, prescription_number, saturday_check, sunday_check, thursday_check, tuesday_check, "version", wednesday_check, week_check, drug_id, patient_id)
VALUES(8, '2019-04-15', '', '2019-04-25', false, true, true, '0008', false, false, false, false, 0, false, false, 1, 6);


INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(1, 'monday', '0001', '9:00', 0, 1);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(2, 'tuesday', '0002', '17:00', 0, 2);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(3, 'wednesday', '0003', '10:00', 0, 3);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(4, 'thursday', '0004', '11:00', 0, 4);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(5, 'week', '0005', '12:00', 0, 5);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(6, 'friday', '0006', '13:00', 0, 6);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(7, 'wednesday', '0002', '9:00', 0, 2);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(8, 'wednesday', '0002', '12:00', 0, 2);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(9, 'wednesday', '0002', '14:00', 0, 2);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(10, 'monday', '0007', '15:00', 0, 7);
INSERT INTO prescription_body(id, "day", prescription_number, reception_time, "version", prescription_id) VALUES(11, 'monday', '0008', '16:00', 0, 8);


INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number) 
VALUES(1, '2020-04-15 09:00:00', 'planned', '', 0, 1, 1, '30', '0001');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(2, '2020-04-15 16:00:00', 'planned', '', 0, 1, 1, '4', '0001');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(3, '2020-04-15 10:00:00', 'planned', '', 0, 2, 1, '7','0002');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(4, '2020-04-15 09:00:00', 'planned', '', 0, 3, 4, '', '0003');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(5, '2020-04-15 10:00:00', 'planned', '', 0, 1, 4, '3', '0004');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(6, '2020-04-15 11:00:00', 'planned', '', 0, 2, 4, '1', '0005');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(7, '2020-04-15 17:00:00', 'planned', '', 0, 1, 4, '5', '0004');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(8, '2020-04-15 09:00:00', 'planned', '', 0, 1, 5, '8', '0006');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(9, '2020-04-15 09:00:00', 'planned', '', 0, 1, 5, '', '0006');
INSERT INTO treatment_event (id, date_of_event, status_of_event, cancel_reason, "version", drug_id, patient_id, dose, prescription_number)
VALUES(10, '2020-04-15 11:00:00', 'canceled', 'Жалоба на головную боль', 0, 1, 1, '', '0001');