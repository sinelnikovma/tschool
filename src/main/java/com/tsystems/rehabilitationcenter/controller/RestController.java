package com.tsystems.rehabilitationcenter.controller;

import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tsystems.rehabilitationcenter.service.TreatmentEventService;
import com.tsystems.rehabilitationcenter.view.DrugDto;
import com.tsystems.rehabilitationcenter.view.PatientDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

@Controller
@RequestMapping(value = "/api")
public class RestController {

	private TreatmentEventService treatmentEventService;

	@Autowired
	public RestController(TreatmentEventService treatmentEventService) {

		this.treatmentEventService = treatmentEventService;
	}

	@GetMapping(value = "/all-treatment-event")
	public @ResponseBody List<TreatmentEventDto> getAllPrescription() {

		TreatmentEventObjectFilter dto = new TreatmentEventObjectFilter();
		dto.setDateFilter(LocalDate.now().toString());
		dto.setDrugFilter(new DrugDto());
		dto.setPatientFilter(new PatientDto());
		dto.setTimeFilter("");

		return treatmentEventService.getTreatmentEventByFilter(dto);
	}

}
