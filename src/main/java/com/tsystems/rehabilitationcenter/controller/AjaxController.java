package com.tsystems.rehabilitationcenter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.tsystems.rehabilitationcenter.service.DrugService;
import com.tsystems.rehabilitationcenter.service.PatientService;
import com.tsystems.rehabilitationcenter.view.DrugDto;
import com.tsystems.rehabilitationcenter.view.PatientDto;

@Controller
@RequestMapping(value = "/ajax")
public class AjaxController {

	private PatientService patientService;
	private DrugService drugService;

	@Autowired
	public AjaxController(PatientService patientService, DrugService drugService) {
		this.patientService = patientService;
		this.drugService = drugService;
	}

	@GetMapping(value = "/getAllPacient", produces = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody List<PatientDto> getPacient(Model model) {

		return patientService.getAllPatient();
	}

	@GetMapping(value = "/getAllDrug")
	public @ResponseBody List<DrugDto> getDrug(Model model) {

		return drugService.getAllDrug();
	}

}
