package com.tsystems.rehabilitationcenter.controller;

import java.security.Principal;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.view.RedirectView;

import com.tsystems.rehabilitationcenter.service.DrugService;
import com.tsystems.rehabilitationcenter.service.PatientService;
import com.tsystems.rehabilitationcenter.service.PhysicianService;
import com.tsystems.rehabilitationcenter.service.PrescriptionService;
import com.tsystems.rehabilitationcenter.view.DrugDto;
import com.tsystems.rehabilitationcenter.view.PatientDto;
import com.tsystems.rehabilitationcenter.view.PhysicianDto;
import com.tsystems.rehabilitationcenter.view.PrescriptionDto;

@Controller
@RequestMapping(value = "/patientaction")
public class PatientController {

	public static final String ALL_DRUG = "allDrug";
	public static final String PATIENT_LIST = "patientList";
	public static final String SHOW_PATIENT_LIST = "patientaction/showPatientList";
	public static final String ADD_PRESCRIPTION = "patientaction/addPrescription";
	public static final String ADD_PATIENT = "patientaction/addPatient";
	public static final String SHOW_PRESCRIPTION_LIST = "patientaction/showPrescriptionList";

	private PatientService patientService;
	private PrescriptionService prescriptionService;
	private DrugService drugService;
	private PhysicianService physicianService;

	@Autowired
	public PatientController(PatientService patientService, PrescriptionService prescriptionService,
			DrugService drugService, PhysicianService physicianService) {
		this.patientService = patientService;
		this.prescriptionService = prescriptionService;
		this.drugService = drugService;
		this.physicianService = physicianService;
	}

	@GetMapping(value = "/showAllPatient")
	public String showAddPatien(Model model) {
		List<PatientDto> allPatient = patientService.getAllPatient();

		model.addAttribute(PATIENT_LIST, allPatient);
		return SHOW_PATIENT_LIST;
	}

	@GetMapping(value = "/dischargePatient")
	public RedirectView dischargePatient(@RequestParam(value = "id") Long id, Model model) {

		if (id > 0) {
			patientService.dischargePatient(id);
		}
		return new RedirectView("showAllPatient");
	}

	@GetMapping(value = "/addPatient")
	public String addPatient(@RequestParam Long id, Model model) {

		if (id > 0) {
			model.addAttribute("patient", patientService.getPatientById(id));
		}

		List<PhysicianDto> allPhysician = physicianService.getPhisicianAll();
		model.addAttribute("doctorList", allPhysician);

		PatientDto patientDto = new PatientDto();
		model.addAttribute("patientDto", patientDto);
		return ADD_PATIENT;
	}

	@PostMapping(value = "/addPatient")
	public String addPatient(@Valid @ModelAttribute("patientDto") PatientDto patientDto, BindingResult result,
			Principal principal, Model model) {

		if (result.hasErrors()) {
			List<PhysicianDto> allPhysician = physicianService.getPhisicianAll();
			model.addAttribute("doctorList", allPhysician);

			return ADD_PATIENT;
		}
		patientService.addPatient(patientDto, principal);

		List<PatientDto> allPatient = patientService.getAllPatient();
		model.addAttribute(PATIENT_LIST, allPatient);

		return SHOW_PATIENT_LIST;
	}

	@GetMapping(value = "/addPrescription")
	public String showAddPrescription(@RequestParam String prescnum, Model model, Principal principal) {

		if (Long.parseLong(prescnum) > 0) {
			PrescriptionDto prescriptionDto = prescriptionService.getPrescriptionByNumber(prescnum);
			model.addAttribute("prescriptionDto", prescriptionDto);
		} else {
			PrescriptionDto prescriptionDto = new PrescriptionDto();
			model.addAttribute("prescriptionDto", prescriptionDto);
		}

		List<DrugDto> allDrug = drugService.getAllDrug();
		model.addAttribute(ALL_DRUG, allDrug);

		List<PatientDto> patientList = patientService.getPatientsByPhysician(principal);
		model.addAttribute(PATIENT_LIST, patientList);

		return ADD_PRESCRIPTION;
	}

	@PostMapping(value = "/addPrescription")
	public String addPrescription(@Valid @ModelAttribute("prescriptionDto") PrescriptionDto prescriptionDto,
			BindingResult result, Model model, Principal principal) {

		if (result.hasErrors()) {
			List<DrugDto> allDrug = drugService.getAllDrug();
			model.addAttribute(ALL_DRUG, allDrug);

			List<PatientDto> patientList = patientService.getPatientsByPhysician(principal);
			model.addAttribute(PATIENT_LIST, patientList);

			return ADD_PRESCRIPTION;
		}
		prescriptionService.createPrescription(prescriptionDto);

		Set<PrescriptionDto> allPrescriptions = prescriptionService.getAllPrescriptions();
		model.addAttribute("prescriptionList", allPrescriptions);

		List<DrugDto> allDrug = drugService.getAllDrug();
		model.addAttribute(ALL_DRUG, allDrug);

		List<PatientDto> patientList = patientService.getPatientsByPhysician(principal);
		model.addAttribute(PATIENT_LIST, patientList);

		return SHOW_PRESCRIPTION_LIST;

	}

	@GetMapping(value = "/showPrescriptionList")
	public String showAllPrescription(@RequestParam(name = "id", required = false, defaultValue = "0") Long id,
			@RequestParam(name = "prescnum", required = false, defaultValue = "0") String prescnum, Model model,
			Principal principal) {

		if (Long.parseLong(prescnum) > 0) {
			PrescriptionDto prescriptionDto = prescriptionService.getPrescriptionByNumber(prescnum);
			prescriptionService.deletePrescription(prescriptionDto);
		}

		Set<PrescriptionDto> prescriptions;
		if (id > 0) {
			prescriptions = prescriptionService.getPrescriptionByPatientId(id);
		} else {
			prescriptions = prescriptionService.getAllPrescriptions();
		}

		model.addAttribute("prescriptionList", prescriptions);

		List<DrugDto> allDrug = drugService.getAllDrug();
		model.addAttribute(ALL_DRUG, allDrug);

		List<PatientDto> patientList = patientService.getPatientsByPhysician(principal);
		model.addAttribute(PATIENT_LIST, patientList);

		return SHOW_PRESCRIPTION_LIST;
	}

	@PostMapping(value = "/showPrescriptionList")
	public String addPrescription(@ModelAttribute("prescriptionList") PrescriptionDto prescriptionDto) {

		return ADD_PRESCRIPTION;
	}

	@GetMapping(value = "/deletePrescription")
	public String deletePrescription(
			@RequestParam(name = "prescnum", required = false, defaultValue = "0") Long prescnum, Model model) {
		return SHOW_PRESCRIPTION_LIST;
	}
}
