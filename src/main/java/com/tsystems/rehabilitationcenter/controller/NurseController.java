package com.tsystems.rehabilitationcenter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/nurse")
public class NurseController {

	@GetMapping(value = "/nurseMain")
	public String getAllTreatment() {

		return "nurse/nurseMain";
	}
}
