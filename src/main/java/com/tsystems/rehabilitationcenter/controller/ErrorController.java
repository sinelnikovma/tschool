package com.tsystems.rehabilitationcenter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping(value = "/error")
public class ErrorController {

	@PostMapping(value = "/accessDenied")
	public String accessDenied() {
		return "/error/accessDenied";
	}
}
