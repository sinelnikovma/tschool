package com.tsystems.rehabilitationcenter.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.tsystems.rehabilitationcenter.service.DrugService;
import com.tsystems.rehabilitationcenter.service.PatientService;
import com.tsystems.rehabilitationcenter.service.TreatmentEventService;
import com.tsystems.rehabilitationcenter.view.DrugDto;
import com.tsystems.rehabilitationcenter.view.PatientDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

@Controller
@RequestMapping(value = "/treatmentEvent")
public class TreatmentEventController {

	public static final String ALL_DRUG = "allDrug";
	public static final String ALL_PATIENT = "allPatient";
	public static final String EVENT_FILTER = "eventFilter";
	public static final String TREATMENT_EVENT = "treatmentEvent/treatmentEvent";
	public static final String TREATMENT_EVENT_LIST = "treatmentEventList";

	private PatientService patientService;
	private DrugService drugService;
	private TreatmentEventService treatmentEventService;

	@Autowired
	public TreatmentEventController(PatientService patientService, DrugService drugService,
			TreatmentEventService treatmentEventService) {
		this.patientService = patientService;
		this.drugService = drugService;
		this.treatmentEventService = treatmentEventService;
	}

	@GetMapping(value = "/treatmentEvent")
	public String showAllEvent(@ModelAttribute(EVENT_FILTER) TreatmentEventObjectFilter treatmentEventFilter,
			Model model) {
		List<PatientDto> allPatient = patientService.getAllPatient();
		model.addAttribute(ALL_PATIENT, allPatient);

		List<DrugDto> allDrug = drugService.getAllDrug();
		model.addAttribute(ALL_DRUG, allDrug);

		TreatmentEventObjectFilter eventFilter = new TreatmentEventObjectFilter();
		model.addAttribute(EVENT_FILTER, eventFilter);

		List<TreatmentEventDto> treatmentEventByFilter = treatmentEventService.getAllTreatmentEvent();
		model.addAttribute(TREATMENT_EVENT_LIST, treatmentEventByFilter);

		return TREATMENT_EVENT;
	}

	@PostMapping(value = "/treatmentEvent")
	public String showEventByFilter(@ModelAttribute(EVENT_FILTER) TreatmentEventObjectFilter treatmentEventFilter,
			BindingResult result, Model model) {

		List<PatientDto> allPatient = patientService.getAllPatient();
		model.addAttribute(ALL_PATIENT, allPatient);

		List<DrugDto> allDrug = drugService.getAllDrug();
		model.addAttribute(ALL_DRUG, allDrug);

		List<TreatmentEventDto> treatmentEventByFilter = treatmentEventService
				.getTreatmentEventByFilter(treatmentEventFilter);
		model.addAttribute(TREATMENT_EVENT_LIST, treatmentEventByFilter);

		return TREATMENT_EVENT;
	}

	@GetMapping(value = "/changeStatus")
	public String getChangeStatus(@RequestParam Long id, Model model) {

		if (id == 0) {
			List<PatientDto> allPatient = patientService.getAllPatient();
			model.addAttribute(ALL_PATIENT, allPatient);

			List<DrugDto> allDrug = drugService.getAllDrug();
			model.addAttribute(ALL_DRUG, allDrug);

			TreatmentEventObjectFilter eventFilter = new TreatmentEventObjectFilter();
			model.addAttribute(EVENT_FILTER, eventFilter);

			List<TreatmentEventDto> treatmentEventByFilter = treatmentEventService.getAllTreatmentEvent();
			model.addAttribute(TREATMENT_EVENT_LIST, treatmentEventByFilter);

			return TREATMENT_EVENT;
		}

		TreatmentEventDto treatmentEvent = treatmentEventService.getTreatmentEventById(id);
		model.addAttribute("treatmentEventDto", treatmentEvent);

		return "treatmentEvent/changeStatus";
	}

	@PostMapping(value = "/changeStatus")
	public String saveChangeStatus(@ModelAttribute("treatmentEventDto") TreatmentEventDto treatmentEventDto,
			@ModelAttribute(EVENT_FILTER) TreatmentEventObjectFilter treatmentEventFilter, BindingResult bindingResult,
			Model model) {

		if (!bindingResult.hasErrors()) {
			treatmentEventService.changeStatus(treatmentEventDto);
		}

		List<PatientDto> allPatient = patientService.getAllPatient();
		model.addAttribute(ALL_PATIENT, allPatient);

		List<DrugDto> allDrug = drugService.getAllDrug();
		model.addAttribute(ALL_DRUG, allDrug);

		TreatmentEventObjectFilter eventFilter = new TreatmentEventObjectFilter();
		model.addAttribute(EVENT_FILTER, eventFilter);

		List<TreatmentEventDto> treatmentEventByFilter = treatmentEventService.getAllTreatmentEvent();
		model.addAttribute(TREATMENT_EVENT_LIST, treatmentEventByFilter);

		return TREATMENT_EVENT;
	}
}
