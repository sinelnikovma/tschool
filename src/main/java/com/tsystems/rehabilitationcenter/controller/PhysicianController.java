package com.tsystems.rehabilitationcenter.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class PhysicianController {

	@GetMapping(value = "/physician/physicianMain")
	public String showPhysicianMain() {
		return "physician/physicianMain";
	}

}
