package com.tsystems.rehabilitationcenter.view;

public class PrescriptionBodyDto {
	private Long id;
	private PrescriptionDto prescriptionId;
	private String prescriptionNumber;
	private String day;
	private String receptionTime;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prescriptionId
	 */

	/**
	 * @return the prescriptionNumber
	 */
	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}

	/**
	 * @return the prescriptionId
	 */
	public PrescriptionDto getPrescriptionId() {
		return prescriptionId;
	}

	/**
	 * @param prescriptionId the prescriptionId to set
	 */
	public void setPrescriptionId(PrescriptionDto prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	/**
	 * @param prescriptionNumber the prescriptionNumber to set
	 */
	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return the receptionTime
	 */
	public String getReceptionTime() {
		return receptionTime;
	}

	/**
	 * @param receptionTime the receptionTime to set
	 */
	public void setReceptionTime(String receptionTime) {
		this.receptionTime = receptionTime;
	}

}
