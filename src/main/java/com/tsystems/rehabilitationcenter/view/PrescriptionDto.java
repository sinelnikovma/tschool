package com.tsystems.rehabilitationcenter.view;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.NotEmpty;

import com.fasterxml.jackson.annotation.JsonBackReference;

public class PrescriptionDto {
	private Long id;

	@NotEmpty(message = "Prescription number cannot be empty")
	private String prescriptionNumber;

	@JsonBackReference
	private PatientDto patient;

	@JsonBackReference
	private DrugDto drugType;

	@NotEmpty(message = "Begin date cannot be empty")
	private String beginDate;

	@NotEmpty(message = "End date cannot be empty")
	private String endDate;
	private Set<String> weekTime = new HashSet<>();
	private Set<String> mondayTime = new HashSet<>();
	private Set<String> tuesdayTime = new HashSet<>();
	private Set<String> wednesdayTime = new HashSet<>();
	private Set<String> thursdayTime = new HashSet<>();
	private Set<String> fridayTime = new HashSet<>();
	private Set<String> saturdayTime = new HashSet<>();
	private Set<String> sundayTime = new HashSet<>();
	private Set<String> weekdays = new HashSet<>(8);
	private String dose;

	/**
	 * @return the weekdays
	 */
	public Set<String> getWeekdays() {
		return weekdays;
	}

	/**
	 * @param weekdays the weekdays to set
	 */
	public void setWeekdays(Set<String> weekdays) {
		this.weekdays = weekdays;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prescriptionNumber
	 */
	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}

	/**
	 * @param prescriptionNumber the prescriptionNumber to set
	 */
	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	/**
	 * @return the patient
	 */
	public PatientDto getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(PatientDto patient) {
		this.patient = patient;
	}

	/**
	 * @return the drugType
	 */
	public DrugDto getDrugType() {
		return drugType;
	}

	/**
	 * @param drugType the drugType to set
	 */
	public void setDrugType(DrugDto drugType) {
		this.drugType = drugType;
	}

	/**
	 * @return the beginDate
	 */
	public String getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(String beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	public String getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the weekTime
	 */
	public Set<String> getWeekTime() {
		return weekTime;
	}

	/**
	 * @param weekTime the weekTime to set
	 */
	public void setWeekTime(Set<String> weekTime) {
		this.weekTime = weekTime;
	}

	/**
	 * @return the mondayTime
	 */
	public Set<String> getMondayTime() {
		return mondayTime;
	}

	/**
	 * @param mondayTime the mondayTime to set
	 */
	public void setMondayTime(Set<String> mondayTime) {
		this.mondayTime = mondayTime;
	}

	/**
	 * @return the thuesdayTime
	 */
	public Set<String> getTuesdayTime() {
		return tuesdayTime;
	}

	/**
	 * @param thuesdayTime the thuesdayTime to set
	 */
	public void setTuesdayTime(Set<String> thuesdayTime) {
		this.tuesdayTime = thuesdayTime;
	}

	/**
	 * @return the wednesdayTime
	 */
	public Set<String> getWednesdayTime() {
		return wednesdayTime;
	}

	/**
	 * @param wednesdayTime the wednesdayTime to set
	 */
	public void setWednesdayTime(Set<String> wednesdayTime) {
		this.wednesdayTime = wednesdayTime;
	}

	/**
	 * @return the thursdayTime
	 */
	public Set<String> getThursdayTime() {
		return thursdayTime;
	}

	/**
	 * @param thursdayTime the thursdayTime to set
	 */
	public void setThursdayTime(Set<String> thursdayTime) {
		this.thursdayTime = thursdayTime;
	}

	/**
	 * @return the frydayTime
	 */
	public Set<String> getFridayTime() {
		return fridayTime;
	}

	/**
	 * @param frydayTime the frydayTime to set
	 */
	public void setFridayTime(Set<String> frydayTime) {
		this.fridayTime = frydayTime;
	}

	/**
	 * @return the saturDayTime
	 */
	public Set<String> getSaturdayTime() {
		return saturdayTime;
	}

	/**
	 * @param saturDayTime the saturDayTime to set
	 */
	public void setSaturdayTime(Set<String> saturDayTime) {
		this.saturdayTime = saturDayTime;
	}

	/**
	 * @return the sundayTime
	 */
	public Set<String> getSundayTime() {
		return sundayTime;
	}

	/**
	 * @param sundayTime the sundayTime to set
	 */
	public void setSundayTime(Set<String> sundayTime) {
		this.sundayTime = sundayTime;
	}

	/**
	 * @return the dose
	 */
	public String getDose() {
		return dose;
	}

	/**
	 * @param dose the dose to set
	 */
	public void setDose(String dose) {
		this.dose = dose;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((prescriptionNumber == null) ? 0 : prescriptionNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrescriptionDto other = (PrescriptionDto) obj;
		if (prescriptionNumber == null) {
			if (other.prescriptionNumber != null)
				return false;
		} else if (!prescriptionNumber.equals(other.prescriptionNumber))
			return false;
		return true;
	}

}
