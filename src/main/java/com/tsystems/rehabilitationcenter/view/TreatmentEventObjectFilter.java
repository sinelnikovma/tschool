package com.tsystems.rehabilitationcenter.view;

public class TreatmentEventObjectFilter {

	private PatientDto patientFilter;
	private DrugDto drugFilter;
	private String dateFilter;
	private String timeFilter;

	/**
	 * @return the patientFilter
	 */
	public PatientDto getPatientFilter() {
		return patientFilter;
	}

	/**
	 * @param patientFilter the patientFilter to set
	 */
	public void setPatientFilter(PatientDto patientFilter) {
		this.patientFilter = patientFilter;
	}

	/**
	 * @return the drugFilter
	 */
	public DrugDto getDrugFilter() {
		return drugFilter;
	}

	/**
	 * @param drugFilter the drugFilter to set
	 */
	public void setDrugFilter(DrugDto drugFilter) {
		this.drugFilter = drugFilter;
	}

	/**
	 * @return the dateFilter
	 */
	public String getDateFilter() {
		return dateFilter;
	}

	/**
	 * @param dateFilter the dateFilter to set
	 */
	public void setDateFilter(String dateFilter) {
		this.dateFilter = dateFilter;
	}

	/**
	 * @return the timeFilter
	 */
	public String getTimeFilter() {
		return timeFilter;
	}

	/**
	 * @param timeFilter the timeFilter to set
	 */
	public void setTimeFilter(String timeFilter) {
		this.timeFilter = timeFilter;
	}

}
