package com.tsystems.rehabilitationcenter.view;

import java.util.HashSet;
import java.util.Set;

import com.fasterxml.jackson.annotation.JsonManagedReference;

public class PhysicianDto {
	private Long id;
	private String physicianFirstName;
	private String physicianLastName;
	private String physicianCode;

	@JsonManagedReference
	private Set<PatientDto> patientList = new HashSet<>();
	private boolean isActive;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the physicianFirstName
	 */
	public String getPhysicianFirstName() {
		return physicianFirstName;
	}

	/**
	 * @param physicianFirstName the physicianFirstName to set
	 */
	public void setPhysicianFirstName(String physicianFirstName) {
		this.physicianFirstName = physicianFirstName;
	}

	/**
	 * @return the physicianLastName
	 */
	public String getPhysicianLastName() {
		return physicianLastName;
	}

	/**
	 * @param physicianLastName the physicianLastName to set
	 */
	public void setPhysicianLastName(String physicianLastName) {
		this.physicianLastName = physicianLastName;
	}

	/**
	 * @return the physicianCode
	 */
	public String getPhysicianCode() {
		return physicianCode;
	}

	/**
	 * @param physicianCode the physicianCode to set
	 */
	public void setPhysicianCode(String physicianCode) {
		this.physicianCode = physicianCode;
	}

	/**
	 * @return the patientList
	 */
	public Set<PatientDto> getPatientList() {
		return patientList;
	}

	/**
	 * @param patientList the patientList to set
	 */
	public void setPatientList(Set<PatientDto> patientList) {
		this.patientList = patientList;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((physicianCode == null) ? 0 : physicianCode.hashCode());
		result = prime * result + ((physicianFirstName == null) ? 0 : physicianFirstName.hashCode());
		result = prime * result + ((physicianLastName == null) ? 0 : physicianLastName.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PhysicianDto other = (PhysicianDto) obj;
		if (physicianCode == null) {
			if (other.physicianCode != null)
				return false;
		} else if (!physicianCode.equals(other.physicianCode))
			return false;
		if (physicianFirstName == null) {
			if (other.physicianFirstName != null)
				return false;
		} else if (!physicianFirstName.equals(other.physicianFirstName))
			return false;
		if (physicianLastName == null) {
			if (other.physicianLastName != null)
				return false;
		} else if (!physicianLastName.equals(other.physicianLastName))
			return false;
		return true;
	}

}
