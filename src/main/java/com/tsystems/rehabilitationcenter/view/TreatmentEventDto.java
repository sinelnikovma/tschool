package com.tsystems.rehabilitationcenter.view;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.ser.std.ToStringSerializer;

public class TreatmentEventDto {

	private Long id;
	private PatientDto patient;

	@JsonSerialize(using = ToStringSerializer.class)
	private LocalDateTime dateOfEvent;

	private String date;
	private String time;

	private String statusOfEvent;
	private String cancelReason;

	private DrugDto drug;
	private String dose;

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the date
	 */
	public String getDate() {

		return dateOfEvent.format(DateTimeFormatter.ofPattern("dd-MM-YYYY"));
	}

	/**
	 * @return the time
	 */
	public String getTime() {

		return dateOfEvent.format(DateTimeFormatter.ofPattern("HH:mm"));
	}

	/**
	 * @return the patient
	 */
	public PatientDto getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(PatientDto patient) {
		this.patient = patient;
	}

	/**
	 * @return the dateOfEvent
	 */
	public LocalDateTime getDateOfEvent() {
		return dateOfEvent;
	}

	/**
	 * @param dateOfEvent the dateOfEvent to set
	 */
	public void setDateOfEvent(LocalDateTime dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}

	/**
	 * @return the statusOfEvent
	 */
	public String getStatusOfEvent() {
		return statusOfEvent;
	}

	/**
	 * @param statusOfEvent the statusOfEvent to set
	 */
	public void setStatusOfEvent(String statusOfEvent) {
		this.statusOfEvent = statusOfEvent;
	}

	/**
	 * @return the cancelReason
	 */
	public String getCancelReason() {
		return cancelReason;
	}

	/**
	 * @param cancelReason the cancelReason to set
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	/**
	 * @return the drug
	 */
	public DrugDto getDrug() {
		return drug;
	}

	/**
	 * @param drug the drug to set
	 */
	public void setDrug(DrugDto drug) {
		this.drug = drug;
	}

	/**
	 * @return the dose
	 */
	public String getDose() {
		return dose;
	}

	/**
	 * @param dose the dose to set
	 */
	public void setDose(String dose) {
		this.dose = dose;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dateOfEvent == null) ? 0 : dateOfEvent.hashCode());
		result = prime * result + ((dose == null) ? 0 : dose.hashCode());
		result = prime * result + ((drug == null) ? 0 : drug.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreatmentEventDto other = (TreatmentEventDto) obj;
		if (dateOfEvent == null) {
			if (other.dateOfEvent != null)
				return false;
		} else if (!dateOfEvent.equals(other.dateOfEvent))
			return false;
		if (dose == null) {
			if (other.dose != null)
				return false;
		} else if (!dose.equals(other.dose))
			return false;
		if (drug == null) {
			if (other.drug != null)
				return false;
		} else if (!drug.equals(other.drug))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		return true;
	}

}
