package com.tsystems.rehabilitationcenter.jms;

import java.util.logging.Level;
import java.util.logging.Logger;

import javax.jms.Destination;
import javax.jms.JMSException;
import javax.jms.Message;
import javax.jms.Session;

import org.springframework.jms.core.JmsTemplate;
import org.springframework.jms.core.MessageCreator;
import org.springframework.stereotype.Component;

@Component
public class JmsInformer {

	private static final Logger logger = Logger.getLogger(JmsInformer.class.getName());
	private JmsTemplate jmsTemplate;
	private Destination defaultDestination;

	/**
	 * @return the jmsTemplate
	 */
	public JmsTemplate getJmsTemplate() {
		return jmsTemplate;
	}

	/**
	 * @param jmsTemplate the jmsTemplate to set
	 */
	public void setJmsTemplate(JmsTemplate jmsTemplate) {
		this.jmsTemplate = jmsTemplate;
	}

	/**
	 * @return the defaultDestination
	 */
	public Destination getDefaultDestination() {
		return defaultDestination;
	}

	/**
	 * @param defaultDestination the defaultDestination to set
	 */
	public void setDefaultDestination(Destination defaultDestination) {
		this.defaultDestination = defaultDestination;
	}

	public void sendMessage(final String msg) {

		String message = "Producer sends " + msg;
		logger.log(Level.INFO, message);

		jmsTemplate.send(defaultDestination, new MessageCreator() {
			public Message createMessage(Session session) throws JMSException {
				return session.createTextMessage(msg);
			}
		});
	}
}
