package com.tsystems.rehabilitationcenter.service;

import java.security.Principal;
import java.util.List;

import com.tsystems.rehabilitationcenter.view.PatientDto;

public interface PatientService {

	public List<PatientDto> getAllPatient();

	public void addPatient(PatientDto patientDto, Principal principal);

	public PatientDto getPatientById(Long id);

	public List<PatientDto> getPatientListByLastName(String lastName);

	public List<PatientDto> getPatientsByPhysician(Principal principal);

	public void dischargePatient(Long id);
}
