package com.tsystems.rehabilitationcenter.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.rehabilitationcenter.dao.TreatmentEventDao;
import com.tsystems.rehabilitationcenter.model.TreatmentEvent;
import com.tsystems.rehabilitationcenter.view.TreatmentEventDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class TreatmentEventServiceImpl implements TreatmentEventService {

	private TreatmentEventDao treatmentEventDao;

	@Autowired
	public TreatmentEventServiceImpl(TreatmentEventDao treatmentEventDao) {
		this.treatmentEventDao = treatmentEventDao;
	}

	@Override
	@Transactional(readOnly = true)
	public List<TreatmentEventDto> getTreatmentEventByFilter(TreatmentEventObjectFilter eventFilter) {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(TreatmentEvent.class, TreatmentEventDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		List<TreatmentEvent> treatmentEventByFilter = treatmentEventDao.getTreatmentEventByFilter(eventFilter);
		List<TreatmentEventDto> treatmentEventDto = new ArrayList<>();

		for (TreatmentEvent treatmentEvent : treatmentEventByFilter) {
			TreatmentEventDto eventDto = mapperFacade.map(treatmentEvent, TreatmentEventDto.class);
			treatmentEventDto.add(eventDto);
		}
		return treatmentEventDto;
	}

	@Override
	@Transactional(readOnly = true)
	public List<TreatmentEventDto> getAllTreatmentEvent() {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(TreatmentEvent.class, TreatmentEventDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		List<TreatmentEvent> treatmentEventByFilter = treatmentEventDao.getAllTreatmentEvent();
		List<TreatmentEventDto> treatmentEventDto = new ArrayList<>();

		for (TreatmentEvent treatmentEvent : treatmentEventByFilter) {
			TreatmentEventDto eventDto = mapperFacade.map(treatmentEvent, TreatmentEventDto.class);
			treatmentEventDto.add(eventDto);
		}
		return treatmentEventDto;
	}

	@Override
	@Transactional
	public void changeStatus(TreatmentEventDto treatmentEventDto) {
		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(TreatmentEventDto.class, TreatmentEvent.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		TreatmentEvent treatmentEvent = mapperFacade.map(treatmentEventDto, TreatmentEvent.class);

		treatmentEventDao.changeStatus(treatmentEvent);

	}

	@Override
	@Transactional(readOnly = true)
	public TreatmentEventDto getTreatmentEventById(Long id) {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(TreatmentEvent.class, TreatmentEventDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		TreatmentEvent treatmentEvent = treatmentEventDao.getTreatmentEventById(id);

		return mapperFacade.map(treatmentEvent, TreatmentEventDto.class);
	}

}
