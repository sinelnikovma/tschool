package com.tsystems.rehabilitationcenter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.rehabilitationcenter.dao.DrugDao;
import com.tsystems.rehabilitationcenter.model.Drug;
import com.tsystems.rehabilitationcenter.view.DrugDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class DrugServiceImlp implements DrugService {

	private DrugDao drugDao;

	@Autowired
	public DrugServiceImlp(DrugDao drugDao) {
		this.drugDao = drugDao;
	}

	@Override
	@Transactional
	public List<DrugDto> getAllDrug() {

		DefaultMapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		mapperFactory.classMap(Drug.class, DrugDto.class).byDefault().register();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		Set<Drug> allDrug = drugDao.getAllDrugs();
		List<DrugDto> drugList = new ArrayList<>();

		for (Drug drug : allDrug) {
			DrugDto drugDto = mapperFacade.map(drug, DrugDto.class);
			drugList.add(drugDto);
		}
		return drugList;
	}

}
