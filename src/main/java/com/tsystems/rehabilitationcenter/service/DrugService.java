package com.tsystems.rehabilitationcenter.service;

import java.util.List;

import com.tsystems.rehabilitationcenter.view.DrugDto;

public interface DrugService {
	public List<DrugDto> getAllDrug();
}
