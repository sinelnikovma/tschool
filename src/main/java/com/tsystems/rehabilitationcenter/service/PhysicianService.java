package com.tsystems.rehabilitationcenter.service;

import java.util.List;

import com.tsystems.rehabilitationcenter.view.PhysicianDto;

public interface PhysicianService {

	public List<PhysicianDto> getPhisicianAll();
}
