package com.tsystems.rehabilitationcenter.service;

import java.util.Set;

import com.tsystems.rehabilitationcenter.view.PrescriptionDto;

public interface PrescriptionService {

	public Set<PrescriptionDto> getAllPrescriptions();

	public PrescriptionDto getPrescriptionByNumber(String number);

	public void createPrescription(PrescriptionDto prescriptionDto);

	public Set<PrescriptionDto> getPrescriptionByPatientId(Long id);

	public void deletePrescription(PrescriptionDto prescriptionDto);

	public PrescriptionDto getPrescriptionById(Long id);
}
