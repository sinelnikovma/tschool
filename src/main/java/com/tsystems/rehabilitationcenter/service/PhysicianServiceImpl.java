package com.tsystems.rehabilitationcenter.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.rehabilitationcenter.dao.PhysicianDao;
import com.tsystems.rehabilitationcenter.model.Physician;
import com.tsystems.rehabilitationcenter.view.PhysicianDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class PhysicianServiceImpl implements PhysicianService {

	private PhysicianDao physicianDao;

	@Autowired
	public PhysicianServiceImpl(PhysicianDao physicianDao) {
		this.physicianDao = physicianDao;
	}

	@Override
	@Transactional
	public List<PhysicianDto> getPhisicianAll() {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(Physician.class, PhysicianDto.class).byDefault().register();

		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Set<Physician> allPhysician = physicianDao.getAllPhysician();
		List<PhysicianDto> physicianDtos = new ArrayList<>();
		for (Physician physician : allPhysician) {
			PhysicianDto mapResult = mapperFacade.map(physician, PhysicianDto.class);
			physicianDtos.add(mapResult);
		}

		return physicianDtos;
	}

}
