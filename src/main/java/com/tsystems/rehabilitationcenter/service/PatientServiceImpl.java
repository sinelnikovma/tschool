package com.tsystems.rehabilitationcenter.service;

import java.security.Principal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.rehabilitationcenter.dao.PatientDao;
import com.tsystems.rehabilitationcenter.dao.PhysicianDao;
import com.tsystems.rehabilitationcenter.dao.PrescriptionBodyDao;
import com.tsystems.rehabilitationcenter.dao.PrescriptionDao;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDao;
import com.tsystems.rehabilitationcenter.jms.JmsInformer;
import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Physician;
import com.tsystems.rehabilitationcenter.model.Prescription;
import com.tsystems.rehabilitationcenter.view.PatientDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class PatientServiceImpl implements PatientService {

	private static final Logger logger = Logger.getLogger(PatientServiceImpl.class.getName());

	private PatientDao patientDao;
	private PhysicianDao physicianDao;
	private TreatmentEventDao treatmentEventDao;
	private PrescriptionDao prescriptionDao;
	private PrescriptionBodyDao prescriptionBodyDao;

	private JmsInformer jmsInformer;

	public PatientServiceImpl() {
	}

	@Autowired
	public PatientServiceImpl(PatientDao patientDao, PhysicianDao physicianDao, TreatmentEventDao treatmentEventDao,
			PrescriptionDao prescriptionDao, PrescriptionBodyDao prescriptionBodyDao) {
		this.patientDao = patientDao;
		this.physicianDao = physicianDao;
		this.treatmentEventDao = treatmentEventDao;
		this.prescriptionDao = prescriptionDao;
		this.prescriptionBodyDao = prescriptionBodyDao;
	}

	@Override
	@Transactional
	public void addPatient(PatientDto patientDto, Principal principal) {

		DefaultMapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		mapperFactory.classMap(PatientDto.class, Patient.class).byDefault().register();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		Patient patient = mapperFacade.map(patientDto, Patient.class);

		// Check if patient already exists
		if (!patient.getInsuranceNumber().isEmpty() && !patient.getPatientLastName().isEmpty()) {
			Patient originalPatient = patientDao.getPatientNyLastnameAndInsuranceNumber(patient);
			if (originalPatient.getId() != null) {
				patient.setId(originalPatient.getId());
			}
		}

		Physician physician = physicianDao.getPhysicianBySecureId(principal.getName());

		patient.setDoctor(physician);
		patient.setStatus("onTreatment");

		if (patientDto.getId() != null) {
			patientDao.updatePatient(patient);
		} else {
			patientDao.createPatient(patient);
		}
	}

	@Override
	@Transactional(readOnly = true)
	public List<PatientDto> getAllPatient() {

		DefaultMapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		mapperFactory.classMap(Patient.class, PatientDto.class).byDefault().register();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		Set<Patient> allPatients = patientDao.getAllPatients();
		List<PatientDto> patientDtos = new ArrayList<>();

		for (Patient patient : allPatients) {
			PatientDto patientDto = mapperFacade.map(patient, PatientDto.class);
			patientDtos.add(patientDto);
		}

		Collections.sort(patientDtos, new Comparator<PatientDto>() {
			public int compare(PatientDto o1, PatientDto o2) {
				return o1.getId().compareTo(o2.getId());
			}
		});

		return patientDtos;
	}

	@Override
	@Transactional(readOnly = true)
	public PatientDto getPatientById(Long id) {

		DefaultMapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		mapperFactory.classMap(Patient.class, PatientDto.class).byDefault().register();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		Patient patient = patientDao.getPatientById(id);

		return mapperFacade.map(patient, PatientDto.class);
	}

	@Override
	@Transactional(readOnly = true)
	public List<PatientDto> getPatientListByLastName(String lastName) {
		logger.log(Level.SEVERE, "function haven't release");

		return Collections.emptyList();
	}

	@Override
	@Transactional(readOnly = true)
	public List<PatientDto> getPatientsByPhysician(Principal principal) {

		DefaultMapperFactory mapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		mapperFactory.classMap(Patient.class, PatientDto.class).byDefault().register();
		MapperFacade mapperFacade = mapperFactory.getMapperFacade();

		Physician physician = physicianDao.getPhysicianBySecureId(principal.getName());
		Set<Patient> patientsByPhysician = patientDao.getPatientsByPhysician(physician);

		List<PatientDto> patientDtos = new ArrayList<>();

		for (Patient patient : patientsByPhysician) {
			PatientDto patientDto = mapperFacade.map(patient, PatientDto.class);
			patientDtos.add(patientDto);
		}

		Collections.sort(patientDtos, new Comparator<PatientDto>() {
			public int compare(PatientDto o1, PatientDto o2) {
				return o1.getId().compareTo(o2.getId());
			}
		});

		return patientDtos;
	}

	@Override
	@Transactional
	public void dischargePatient(Long id) {
		Patient patient = patientDao.getPatientById(id);
		treatmentEventDao.deleteTreatmentEventByPatient(patient);
		Set<Prescription> set = prescriptionDao.getPrescriptionsByPatient(id);

		for (Prescription prescription : set) {
			prescriptionBodyDao.deletePrescriptionBodyByNumber(prescription.getPrescriptionNumber());
//			prescriptionDao.deletePrescription(prescription);
		}

		patientDao.deletePatient(id);

		sendMessage2MQ();

	}

	/**
	 * Send message to MQ server
	 */
	private void sendMessage2MQ() {
		if (this.jmsInformer == null) {
			this.jmsInformer = getJmsInformer();
		}
		if (this.jmsInformer != null) {
			jmsInformer.sendMessage("update");
		}
	}

	/**
	 * Try to create jmsInformer from file and assign it
	 * 
	 * @return
	 */
	private JmsInformer getJmsInformer() {

		JmsInformer jmsSender = null;
		try {
			ApplicationContext appContext = new ClassPathXmlApplicationContext("jms-context.xml");

			jmsSender = (JmsInformer) appContext.getBean("jmsSender");

			jmsSender.sendMessage("update");
		} catch (Exception e) {
			logger.log(Level.WARNING, "Remote MQ server have not answered. {0}", e.getMessage());
		}
		return jmsSender;
	}

}
