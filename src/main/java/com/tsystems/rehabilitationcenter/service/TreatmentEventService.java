package com.tsystems.rehabilitationcenter.service;

import java.util.List;

import com.tsystems.rehabilitationcenter.view.TreatmentEventDto;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

public interface TreatmentEventService {

	public List<TreatmentEventDto> getTreatmentEventByFilter(TreatmentEventObjectFilter eventFilter);

	public List<TreatmentEventDto> getAllTreatmentEvent();

	public void changeStatus(TreatmentEventDto treatmentEventDto);

	public TreatmentEventDto getTreatmentEventById(Long id);
}
