package com.tsystems.rehabilitationcenter.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.tsystems.rehabilitationcenter.dao.DrugDao;
import com.tsystems.rehabilitationcenter.dao.PatientDao;
import com.tsystems.rehabilitationcenter.dao.PrescriptionBodyDao;
import com.tsystems.rehabilitationcenter.dao.PrescriptionDao;
import com.tsystems.rehabilitationcenter.dao.TreatmentEventDao;
import com.tsystems.rehabilitationcenter.jms.JmsInformer;
import com.tsystems.rehabilitationcenter.model.Drug;
import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Prescription;
import com.tsystems.rehabilitationcenter.model.PrescriptionBody;
import com.tsystems.rehabilitationcenter.model.TreatmentEvent;
import com.tsystems.rehabilitationcenter.view.PrescriptionBodyDto;
import com.tsystems.rehabilitationcenter.view.PrescriptionDto;

import ma.glasnost.orika.MapperFacade;
import ma.glasnost.orika.impl.DefaultMapperFactory;

@Service
public class PrescriptionServiceImpl implements PrescriptionService {

	public static final String TIME_FORMAT = "HH:mm";
	public static final String DAY_MONDAY = "monday";
	public static final String DAY_TUESDAY = "tuesday";
	public static final String DAY_WEDNESDAY = "wednesday";
	public static final String DAY_THURSDAY = "thursday";
	public static final String DAY_FRIDAY = "friday";
	public static final String DAY_SATURDAY = "saturday";
	public static final String DAY_SUNDAY = "sunday";
	public static final String STATUS_PLANNED = "planned";
	public static final String WHOLE_WEEK = "week";

	private static final Logger logger = Logger.getLogger(PrescriptionServiceImpl.class.getName());

	private PrescriptionDao prescriptionDao;
	private PrescriptionBodyDao prescriptionBodyDao;
	private PatientDao patientDao;
	private DrugDao drugDao;
	private TreatmentEventDao treatmentEventDao;

	private JmsInformer jmsInformer;

	@Autowired
	public PrescriptionServiceImpl(PrescriptionDao prescriptionDao, PatientDao patientDao, DrugDao drugDao,
			TreatmentEventDao treatmentEventDao, PrescriptionBodyDao prescriptionBodyDao) {
		this.prescriptionDao = prescriptionDao;

		this.patientDao = patientDao;
		this.drugDao = drugDao;
		this.treatmentEventDao = treatmentEventDao;
		this.prescriptionBodyDao = prescriptionBodyDao;

		sendMessage2MQ();
	}

	@Override
	@Transactional(readOnly = true)
	public Set<PrescriptionDto> getAllPrescriptions() {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(Prescription.class, PrescriptionDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Set<Prescription> allPrescriptions = prescriptionDao.getAllPrescriptions();
		Set<PrescriptionDto> allPrescriptionsDto = new HashSet<>();

		for (Prescription prescription : allPrescriptions) {

			PrescriptionDto dto = mapperFacade.map(prescription, PrescriptionDto.class);
			allPrescriptionsDto.add(dto);
		}

		return allPrescriptionsDto;
	}

	@Override
	@Transactional(readOnly = true)
	public PrescriptionDto getPrescriptionByNumber(String number) {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(Prescription.class, PrescriptionDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Prescription prescription = prescriptionDao.getPrescriptionByNumber(number);
		Set<PrescriptionBody> allPrescriptionBodies = prescription.getPrescriptionBody();

		PrescriptionDto prescriptionDto = mapperFacade.map(prescription, PrescriptionDto.class);
		defaultMapperFactory.classMap(PrescriptionBody.class, PrescriptionBodyDto.class);
		mapperFacade = defaultMapperFactory.getMapperFacade();

		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TIME_FORMAT);

		for (PrescriptionBody prescriptionBody : allPrescriptionBodies) {
			PrescriptionBodyDto prescriptionBodyDto = mapperFacade.map(prescriptionBody, PrescriptionBodyDto.class);

			LocalTime receptionTime = prescriptionBody.getReceptionTime();
			String formattedTime = receptionTime.format(formatter);
			String day = prescriptionBodyDto.getDay();

			fillTimesArray(day, formattedTime, prescriptionDto);

		}

		fillDaysCheckboxes(prescription, prescriptionDto);

		return prescriptionDto;
	}

	@Override
	@Transactional(readOnly = true)
	public Set<PrescriptionDto> getPrescriptionByPatientId(Long id) {
		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(Prescription.class, PrescriptionDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Set<Prescription> allPrescriptions = prescriptionDao.getPrescriptionsByPatient(id);
		Set<PrescriptionDto> allPrescriptionsDto = new HashSet<>();

		for (Prescription prescription : allPrescriptions) {

			PrescriptionDto dto = mapperFacade.map(prescription, PrescriptionDto.class);
			allPrescriptionsDto.add(dto);
		}

		return allPrescriptionsDto;

	}

	@Override
	@Transactional(readOnly = true)
	public PrescriptionDto getPrescriptionById(Long id) {
		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(Prescription.class, PrescriptionDto.class);
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Prescription prescriptionById = prescriptionDao.getPrescriptionById(id);

		return mapperFacade.map(prescriptionById, PrescriptionDto.class);
	}

	/**
	 *
	 */
	@Override
	@Transactional
	public void createPrescription(PrescriptionDto prescriptionDto) {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(PrescriptionDto.class, Prescription.class).exclude("beginDate").exclude("endDate")
				.byDefault().register();
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Prescription prescription = mapperFacade.map(prescriptionDto, Prescription.class);
		fillBooleanDaysFromSet(prescriptionDto, prescription);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

		Prescription prescriptionByNumber = prescriptionDao
				.getPrescriptionByNumber(prescriptionDto.getPrescriptionNumber());

		// checking number exist
		if (prescriptionByNumber != null) {
			prescription.setId(prescriptionByNumber.getId());
		}

		prescription.setBeginDate(LocalDate.parse(prescriptionDto.getBeginDate(), formatter));
		prescription.setEndDate(LocalDate.parse(prescriptionDto.getEndDate(), formatter));

		Patient patientById = patientDao.getPatientById(prescriptionDto.getPatient().getId());
		prescription.setPatient(patientById);
		prescription.setDrugType(drugDao.getDrugById(prescriptionDto.getDrugType().getId()));
		prescription.setActive(true);

		Long prescriptionId = prescription.getId();
		Prescription prescriptionById;
		if (prescriptionId != null) {
			prescriptionById = prescriptionDao.getPrescriptionById(prescriptionId);
		} else {
			prescriptionById = prescription;

		}

		Set<PrescriptionBody> prescriptionBodyList = new HashSet<>();
		formatter = DateTimeFormatter.ofPattern(TIME_FORMAT);
		Set<PrescriptionBodyDto> bodyDtos = getPrescriptionDtoBodiesFromPrescriptionDto(prescriptionDto);

		for (PrescriptionBodyDto prescriptionBodyDto : bodyDtos) {

			PrescriptionBody prescriptionBody = new PrescriptionBody();
			prescriptionBody.setId(prescriptionBodyDto.getId());
			prescriptionBody.setPrescriptionId(prescriptionById);
			prescriptionBody.setPrescriptionNumber(prescriptionBodyDto.getPrescriptionNumber());
			prescriptionBody.setDay(prescriptionBodyDto.getDay());
			prescriptionBody.setReceptionTime(LocalTime.parse(prescriptionBodyDto.getReceptionTime()));
			prescriptionBodyList.add(prescriptionBody);
		}

		prescription.setPrescriptionBody(prescriptionBodyList);

		if (prescriptionId != null) {
			updatePrescription(prescription);
		} else {
			createPrescription(prescription);
		}

		generateTreatmentEvent(prescriptionDto, prescription);

		sendMessage2MQ();

	}

	@Override
	@Transactional
	public void deletePrescription(PrescriptionDto prescriptionDto) {

		DefaultMapperFactory defaultMapperFactory = new DefaultMapperFactory.Builder().mapNulls(false).build();
		defaultMapperFactory.classMap(PrescriptionDto.class, Prescription.class).exclude("beginDate").exclude("endDate")
				.byDefault().register();
		MapperFacade mapperFacade = defaultMapperFactory.getMapperFacade();

		Prescription prescription = mapperFacade.map(prescriptionDto, Prescription.class);

		treatmentEventDao.deleteTreatmentEventByPrescriptionNumber(prescription.getPrescriptionNumber());
		prescriptionBodyDao.deletePrescriptionBodyByNumber(prescription.getPrescriptionNumber());
		prescriptionDao.deletePrescription(prescription);

		sendMessage2MQ();

	}

	/**
	 * Send message to MQ server
	 */
	private void sendMessage2MQ() {
		if (this.jmsInformer == null) {
			this.jmsInformer = getJmsInformer();
		}
		if (this.jmsInformer != null) {
			jmsInformer.sendMessage("update");
		}
	}

	/**
	 * Try to create jmsInformer from file and assign it
	 * 
	 * @return
	 */
	private JmsInformer getJmsInformer() {

		JmsInformer jmsSender = null;
		try {
			ApplicationContext appContext = new ClassPathXmlApplicationContext("jms-context.xml");

			jmsSender = (JmsInformer) appContext.getBean("jmsSender");

			jmsSender.sendMessage("update");
		} catch (Exception e) {
			logger.log(Level.WARNING, "Remote MQ server have not answered. {0}", e.getMessage());
		}
		return jmsSender;
	}

	/**
	 * Create Prescription from scratch
	 * 
	 * @param prescription
	 */
	private void createPrescription(Prescription prescription) {
		prescriptionDao.createPrescrition(prescription);
	}

	/**
	 * Update existed Prescription
	 * 
	 * @param prescription
	 */
	private void updatePrescription(Prescription prescription) {
		prescriptionDao.updatePrescription(prescription);
	}

	/**
	 * Generate treatment events
	 * 
	 * @param prescription
	 */
	private void generateTreatmentEvent(PrescriptionDto prescriptionDto, Prescription prescription) {

		List<TreatmentEvent> treatmentEventList = treatmentEventDao
				.getTreatmentEventByPrescriptionNumber(prescriptionDto.getPrescriptionNumber());

		if (!treatmentEventList.isEmpty()) {
			treatmentEventDao.deleteTreatmentEventByPrescriptionNumber(prescriptionDto.getPrescriptionNumber());
		}

		createTreatmentEventList(prescriptionDto, prescription);
	}

	/**
	 * Auxiliary function
	 * 
	 * @param prescription
	 */
	private void createTreatmentEventList(PrescriptionDto prescriptionDto, Prescription actualPrescription) {

		LocalDate beginDate = LocalDate.parse(prescriptionDto.getBeginDate());
		LocalDate endDate = LocalDate.parse(prescriptionDto.getEndDate());

		LocalDate activeDate;
		Set<String> weekday = prescriptionDto.getWeekdays();

		Patient patient = actualPrescription.getPatient();
		Drug drug = actualPrescription.getDrugType();
		String dose = actualPrescription.getDose();

		long period = ChronoUnit.DAYS.between(beginDate, endDate);
		DateTimeFormatter formatter = DateTimeFormatter.ofPattern(TIME_FORMAT);
		Set<String> timeList;

		for (int i = 0; i <= period; i++) {

			activeDate = beginDate.plusDays(i);

			String dayOfWeek = activeDate.getDayOfWeek().name().toLowerCase();

			if (weekday.contains(WHOLE_WEEK)) {
				timeList = prescriptionDto.getWeekTime();

				for (String time : timeList) {

					LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

					TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
							localDateTime, STATUS_PLANNED, "", drug, dose);

					treatmentEventDao.createTreatmentEvent(event);
				}
			} else {
				switch (dayOfWeek) {
				case DAY_MONDAY:
					if (weekday.contains(DAY_MONDAY)) {
						timeList = prescriptionDto.getMondayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				case DAY_TUESDAY:
					if (weekday.contains(DAY_TUESDAY)) {
						timeList = prescriptionDto.getTuesdayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				case DAY_WEDNESDAY:
					if (weekday.contains(DAY_WEDNESDAY)) {
						timeList = prescriptionDto.getWednesdayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				case DAY_THURSDAY:
					if (weekday.contains(DAY_THURSDAY)) {
						timeList = prescriptionDto.getThursdayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				case DAY_FRIDAY:
					if (weekday.contains(DAY_FRIDAY)) {
						timeList = prescriptionDto.getFridayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				case DAY_SATURDAY:
					if (weekday.contains(DAY_SATURDAY)) {
						timeList = prescriptionDto.getSaturdayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				case DAY_SUNDAY:
					if (weekday.contains(DAY_SUNDAY)) {
						timeList = prescriptionDto.getSundayTime();

						for (String time : timeList) {

							LocalDateTime localDateTime = activeDate.atTime(LocalTime.parse(time, formatter));

							TreatmentEvent event = new TreatmentEvent(prescriptionDto.getPrescriptionNumber(), patient,
									localDateTime, STATUS_PLANNED, "", drug, dose);

							treatmentEventDao.createTreatmentEvent(event);
						}
					}
					break;

				default:
					break;
				}
			}
		}
	}

	/**
	 * Transformation set of included days to simple boolean variables
	 * 
	 * @param prescriptionDto
	 * @param prescription
	 */
	private void fillBooleanDaysFromSet(PrescriptionDto prescriptionDto, Prescription prescription) {

		Set<String> weekdays = prescriptionDto.getWeekdays();

		if (weekdays.contains(WHOLE_WEEK)) {
			prescription.setWeekCheck(true);
		}
		if (weekdays.contains(DAY_MONDAY)) {
			prescription.setMondayCheck(true);
		}
		if (weekdays.contains(DAY_TUESDAY)) {
			prescription.setTuesdayCheck(true);
		}
		if (weekdays.contains(DAY_WEDNESDAY)) {
			prescription.setWednesdayCheck(true);
		}
		if (weekdays.contains(DAY_THURSDAY)) {
			prescription.setThursdayCheck(true);
		}
		if (weekdays.contains(DAY_FRIDAY)) {
			prescription.setFridayCheck(true);
		}
		if (weekdays.contains(DAY_SATURDAY)) {
			prescription.setSaturdayCheck(true);
		}
		if (weekdays.contains(DAY_SUNDAY)) {
			prescription.setSundayCheck(true);
		}
	}

	/**
	 * Extract time array from parent to body dto
	 * 
	 * @param prescriptionDto
	 * @return
	 */
	private Set<PrescriptionBodyDto> getPrescriptionDtoBodiesFromPrescriptionDto(PrescriptionDto prescriptionDto) {

		Set<PrescriptionBodyDto> list = new HashSet<>();

		Set<String> set = prescriptionDto.getWeekTime();

		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, WHOLE_WEEK);
		}

		set = prescriptionDto.getMondayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_MONDAY);
		}

		set = prescriptionDto.getTuesdayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_TUESDAY);
		}

		set = prescriptionDto.getWednesdayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_WEDNESDAY);
		}

		set = prescriptionDto.getThursdayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_THURSDAY);
		}

		set = prescriptionDto.getFridayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_FRIDAY);
		}

		set = prescriptionDto.getSaturdayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_SATURDAY);
		}

		set = prescriptionDto.getSundayTime();
		if (!set.isEmpty()) {
			fillPrescriptionBodyDto(prescriptionDto, list, set, DAY_SUNDAY);
		}

		return list;
	}

	/**
	 * Create row of prescription body for specific day
	 * 
	 * @param prescriptionDto
	 * @param list
	 * @param set
	 * @param day
	 */
	private void fillPrescriptionBodyDto(PrescriptionDto prescriptionDto, Set<PrescriptionBodyDto> list,
			Set<String> set, String day) {
		for (String time : set) {
			PrescriptionBodyDto prescriptionBodyDto = new PrescriptionBodyDto();
			prescriptionBodyDto.setPrescriptionNumber(prescriptionDto.getPrescriptionNumber());
			prescriptionBodyDto.setDay(day);
			prescriptionBodyDto.setPrescriptionId(prescriptionDto);
			prescriptionBodyDto.setReceptionTime(time);

			list.add(prescriptionBodyDto);
		}
	}

	/**
	 * Populating set collection by checked days
	 * 
	 * @param prescription
	 * @param prescriptionDto
	 */
	private void fillDaysCheckboxes(Prescription prescription, PrescriptionDto prescriptionDto) {
		if (prescription.isWeekCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(WHOLE_WEEK);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isMondayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_MONDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isTuesdayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_TUESDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isWednesdayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_WEDNESDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isThursdayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_THURSDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isFridayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_FRIDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isSaturdayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_SATURDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
		if (prescription.isSundayCheck()) {
			Set<String> weekdays = prescriptionDto.getWeekdays();
			weekdays.add(DAY_SUNDAY);
			prescriptionDto.setWeekdays(weekdays);
		}
	}

	/**
	 * Populating timelist by times of week days
	 * 
	 * @param day
	 * @param formattedTime
	 * @param temporaryDto
	 */
	private void fillTimesArray(String day, String formattedTime, PrescriptionDto temporaryDto) {
		switch (day) {
		case WHOLE_WEEK:
			Set<String> weekSet = temporaryDto.getWeekTime();
			weekSet.add(formattedTime);
			temporaryDto.setWeekTime(weekSet);
			break;
		case DAY_MONDAY:
			Set<String> mondaySet = temporaryDto.getMondayTime();
			mondaySet.add(formattedTime);
			temporaryDto.setMondayTime(mondaySet);
			break;
		case DAY_TUESDAY:
			Set<String> tuesdaySet = temporaryDto.getTuesdayTime();
			tuesdaySet.add(formattedTime);
			temporaryDto.setTuesdayTime(tuesdaySet);
			break;
		case DAY_WEDNESDAY:
			Set<String> wednesdaySet = temporaryDto.getWednesdayTime();
			wednesdaySet.add(formattedTime);
			temporaryDto.setWednesdayTime(wednesdaySet);
			break;
		case DAY_THURSDAY:
			Set<String> thursdaySet = temporaryDto.getThursdayTime();
			thursdaySet.add(formattedTime);
			temporaryDto.setThursdayTime(thursdaySet);
			break;
		case DAY_FRIDAY:
			Set<String> fridaySet = temporaryDto.getFridayTime();
			fridaySet.add(formattedTime);
			temporaryDto.setFridayTime(fridaySet);
			break;
		case DAY_SATURDAY:
			Set<String> saturdaySet = temporaryDto.getSaturdayTime();
			saturdaySet.add(formattedTime);
			temporaryDto.setSaturdayTime(saturdaySet);
			break;
		case DAY_SUNDAY:
			Set<String> sundaySet = temporaryDto.getSundayTime();
			sundaySet.add(formattedTime);
			temporaryDto.setSundayTime(sundaySet);
			break;
		default:
			break;
		}
	}

}
