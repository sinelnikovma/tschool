package com.tsystems.rehabilitationcenter.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "patient")
public class Patient {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "patient_first_name")
	private String patientFirstName;

	@Column(name = "patient_last_name")
	private String patientLastName;

	@Column(name = "diagnosis")
	private String diagnosis;

	@Column(name = "insurance_number")
	private String insuranceNumber;

	@ManyToOne
	@JoinColumn(name = "doctor_id")
	@JsonManagedReference
	private Physician doctor;

	@OneToMany(mappedBy = "patient", cascade = CascadeType.ALL)
	@JsonManagedReference
	private List<Prescription> prescriptionList = new ArrayList<>();

	@OneToMany(mappedBy = "patient")
	@JsonManagedReference
	private List<TreatmentEvent> treatmentEventList = new ArrayList<>();

	@Column(name = "status")
	private String status;

	@Column(name = "active")
	private boolean isActive;

	public Patient() {
	}

	public Patient(String patientFirstName, String patientLastName, String diagnosis, String insuranceNumber,
			Physician doctor, List<Prescription> prescriptionList, List<TreatmentEvent> treatmentEventList,
			String status, boolean isActive) {
		this.patientFirstName = patientFirstName;
		this.patientLastName = patientLastName;
		this.diagnosis = diagnosis;
		this.insuranceNumber = insuranceNumber;
		this.doctor = doctor;
		this.prescriptionList = prescriptionList;
		this.treatmentEventList = treatmentEventList;
		this.status = status;
		this.isActive = isActive;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the patientFirstName
	 */
	public String getPatientFirstName() {
		return patientFirstName;
	}

	/**
	 * @param patientFirstName the patientFirstName to set
	 */
	public void setPatientFirstName(String patientFirstName) {
		this.patientFirstName = patientFirstName;
	}

	/**
	 * @return the patientLastName
	 */
	public String getPatientLastName() {
		return patientLastName;
	}

	/**
	 * @param patientLastName the patientLastName to set
	 */
	public void setPatientLastName(String patientLastName) {
		this.patientLastName = patientLastName;
	}

	/**
	 * @return the diagnosis
	 */
	public String getDiagnosis() {
		return diagnosis;
	}

	/**
	 * @param diagnosis the diagnosis to set
	 */
	public void setDiagnosis(String diagnosis) {
		this.diagnosis = diagnosis;
	}

	/**
	 * @return the insuranceNumber
	 */
	public String getInsuranceNumber() {
		return insuranceNumber;
	}

	/**
	 * @param insuranceNumber the insuranceNumber to set
	 */
	public void setInsuranceNumber(String insuranceNumber) {
		this.insuranceNumber = insuranceNumber;
	}

	/**
	 * @return the doctor
	 */
	public Physician getDoctor() {
		return doctor;
	}

	/**
	 * @param doctor the doctor to set
	 */
	public void setDoctor(Physician doctor) {
		this.doctor = doctor;
	}

	/**
	 * @return the prescriptionList
	 */
	public List<Prescription> getPrescriptionList() {
		return prescriptionList;
	}

	/**
	 * @param prescriptionList the prescriptionList to set
	 */
	public void setPrescriptionList(List<Prescription> prescriptionList) {
		this.prescriptionList = prescriptionList;
	}

	/**
	 * @return the treatmentEventList
	 */
	public List<TreatmentEvent> getTreatmentEventList() {
		return treatmentEventList;
	}

	/**
	 * @param treatmentEventList the treatmentEventList to set
	 */
	public void setTreatmentEventList(List<TreatmentEvent> treatmentEventList) {
		this.treatmentEventList = treatmentEventList;
	}

	/**
	 * @return the status
	 */
	public String getStatus() {
		return status;
	}

	/**
	 * @param status the status to set
	 */
	public void setStatus(String status) {
		this.status = status;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((insuranceNumber == null) ? 0 : insuranceNumber.hashCode());
		result = prime * result + ((patientFirstName == null) ? 0 : patientFirstName.hashCode());
		result = prime * result + ((patientLastName == null) ? 0 : patientLastName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Patient other = (Patient) obj;
		if (insuranceNumber == null) {
			if (other.insuranceNumber != null)
				return false;
		} else if (!insuranceNumber.equals(other.insuranceNumber))
			return false;
		if (patientFirstName == null) {
			if (other.patientFirstName != null)
				return false;
		} else if (!patientFirstName.equals(other.patientFirstName))
			return false;
		if (patientLastName == null) {
			if (other.patientLastName != null)
				return false;
		} else if (!patientLastName.equals(other.patientLastName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Patient [id=" + id + ", patientFirstName=" + patientFirstName + ", patientLastName=" + patientLastName
				+ ", diagnosis=" + diagnosis + ", insuranceNumber=" + insuranceNumber + ", doctor=" + doctor
				+ ", status=" + status + ", isActive=" + isActive + "]";
	}

}
