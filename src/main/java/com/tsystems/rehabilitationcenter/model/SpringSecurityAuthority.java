package com.tsystems.rehabilitationcenter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "authorities", uniqueConstraints = @UniqueConstraint(columnNames = { "authority", "username" }))
public class SpringSecurityAuthority {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "user_role_id", unique = true, nullable = false)
	private Integer userRoleId;

	@ManyToOne
	@JoinColumn(name = "username", nullable = false)
	private SpringSecurityUsers user;

	@Column(name = "authority", nullable = false, length = 50)
	private String userRole;

	public SpringSecurityAuthority() {
		// constructor
	}

}
