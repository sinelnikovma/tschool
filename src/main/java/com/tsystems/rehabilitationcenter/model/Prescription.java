package com.tsystems.rehabilitationcenter.model;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "prescription")
public class Prescription {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "prescription_number", nullable = false)
	private String prescriptionNumber;

	@ManyToOne
	@JoinColumn(name = "patient_id")
	@JsonManagedReference
	private Patient patient;

	@OneToOne()
	@JoinColumn(name = "drug_id")
	private Drug drugType;

	@Column(name = "begin_date")
	private LocalDate beginDate;

	@Column(name = "end_date")
	private LocalDate endDate;

	@OneToMany(mappedBy = "prescriptionId", cascade = CascadeType.ALL)
	@JsonBackReference
	private Set<PrescriptionBody> prescriptionBody = new HashSet<>();

	@Column(name = "week_check")
	private boolean weekCheck;

	@Column(name = "monday_check")
	private boolean mondayCheck;

	@Column(name = "tuesday_check")
	private boolean tuesdayCheck;

	@Column(name = "wednesday_check")
	private boolean wednesdayCheck;

	@Column(name = "thursday_check")
	private boolean thursdayCheck;

	@Column(name = "friday_check")
	private boolean fridayCheck;

	@Column(name = "saturday_check")
	private boolean saturdayCheck;

	@Column(name = "sunday_check")
	private boolean sundayCheck;

	@Column(name = "dose")
	private String dose;

	@Column(name = "active")
	private boolean isActive;

	public Prescription() {
	}

	public Prescription(String prescriptionNumber, Patient patient, Drug drugType, LocalDate beginDate,
			LocalDate endDate, Set<PrescriptionBody> prescriptionBody, boolean weekCheck, boolean mondayCheck,
			boolean tuesdayCheck, boolean wednesdayCheck, boolean thursdayCheck, boolean fridayCheck,
			boolean saturdayCheck, boolean sundayCheck, String dose, boolean isActive) {
		this.prescriptionNumber = prescriptionNumber;
		this.patient = patient;
		this.drugType = drugType;
		this.beginDate = beginDate;
		this.endDate = endDate;
		this.prescriptionBody = prescriptionBody;
		this.weekCheck = weekCheck;
		this.mondayCheck = mondayCheck;
		this.tuesdayCheck = tuesdayCheck;
		this.wednesdayCheck = wednesdayCheck;
		this.thursdayCheck = thursdayCheck;
		this.fridayCheck = fridayCheck;
		this.saturdayCheck = saturdayCheck;
		this.sundayCheck = sundayCheck;
		this.dose = dose;
		this.isActive = isActive;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prescriptionNumber
	 */
	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}

	/**
	 * @param prescriptionNumber the prescriptionNumber to set
	 */
	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @return the drugType
	 */
	public Drug getDrugType() {
		return drugType;
	}

	/**
	 * @param drugType the drugType to set
	 */
	public void setDrugType(Drug drugType) {
		this.drugType = drugType;
	}

	/**
	 * @return the beginDate
	 */
	public LocalDate getBeginDate() {
		return beginDate;
	}

	/**
	 * @param beginDate the beginDate to set
	 */
	public void setBeginDate(LocalDate beginDate) {
		this.beginDate = beginDate;
	}

	/**
	 * @return the endDate
	 */
	public LocalDate getEndDate() {
		return endDate;
	}

	/**
	 * @param endDate the endDate to set
	 */
	public void setEndDate(LocalDate endDate) {
		this.endDate = endDate;
	}

	/**
	 * @return the prescriptionBody
	 */
	public Set<PrescriptionBody> getPrescriptionBody() {
		return prescriptionBody;
	}

	/**
	 * @param prescriptionBody the prescriptionBody to set
	 */
	public void setPrescriptionBody(Set<PrescriptionBody> prescriptionBody) {
		if (this.prescriptionBody != null && !this.prescriptionBody.isEmpty()) {
			this.prescriptionBody.clear();
			this.prescriptionBody.addAll(prescriptionBody);
		} else {
			this.prescriptionBody = prescriptionBody;
		}

	}

	/**
	 * @param weekCheck the weekCheck to set
	 */
	public void setWeekCheck(boolean weekCheck) {
		this.weekCheck = weekCheck;
	}

	/**
	 * @return the weekCheck
	 */
	public boolean isWeekCheck() {
		return weekCheck;
	}

	/**
	 * @return the mondayCheck
	 */
	public boolean isMondayCheck() {
		return mondayCheck;
	}

	/**
	 * @param mondayCheck the mondayCheck to set
	 */
	public void setMondayCheck(boolean mondayCheck) {
		this.mondayCheck = mondayCheck;
	}

	/**
	 * @return the tuesdayCheck
	 */
	public boolean isTuesdayCheck() {
		return tuesdayCheck;
	}

	/**
	 * @param tuesdayCheck the tuesdayCheck to set
	 */
	public void setTuesdayCheck(boolean tuesdayCheck) {
		this.tuesdayCheck = tuesdayCheck;
	}

	/**
	 * @return the wednesdayCheck
	 */
	public boolean isWednesdayCheck() {
		return wednesdayCheck;
	}

	/**
	 * @param wednesdayCheck the wednesdayCheck to set
	 */
	public void setWednesdayCheck(boolean wednesdayCheck) {
		this.wednesdayCheck = wednesdayCheck;
	}

	/**
	 * @return the thursdayCheck
	 */
	public boolean isThursdayCheck() {
		return thursdayCheck;
	}

	/**
	 * @param thursdayCheck the thursdayCheck to set
	 */
	public void setThursdayCheck(boolean thursdayCheck) {
		this.thursdayCheck = thursdayCheck;
	}

	/**
	 * @return the fridayCheck
	 */
	public boolean isFridayCheck() {
		return fridayCheck;
	}

	/**
	 * @param fridayCheck the fridayCheck to set
	 */
	public void setFridayCheck(boolean fridayCheck) {
		this.fridayCheck = fridayCheck;
	}

	/**
	 * @return the saturdayCheck
	 */
	public boolean isSaturdayCheck() {
		return saturdayCheck;
	}

	/**
	 * @param saturdayCheck the saturdayCheck to set
	 */
	public void setSaturdayCheck(boolean saturdayCheck) {
		this.saturdayCheck = saturdayCheck;
	}

	/**
	 * @return the sundayCheck
	 */
	public boolean isSundayCheck() {
		return sundayCheck;
	}

	/**
	 * @param sundayCheck the sundayCheck to set
	 */
	public void setSundayCheck(boolean sundayCheck) {
		this.sundayCheck = sundayCheck;
	}

	/**
	 * @return the dose
	 */
	public String getDose() {
		return dose;
	}

	/**
	 * @param dose the dose to set
	 */
	public void setDose(String dose) {
		this.dose = dose;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((prescriptionNumber == null) ? 0 : prescriptionNumber.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Prescription other = (Prescription) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (prescriptionNumber == null) {
			if (other.prescriptionNumber != null)
				return false;
		} else if (!prescriptionNumber.equals(other.prescriptionNumber))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Prescription [id=" + id + ", prescriptionNumber=" + prescriptionNumber + ", beginDate=" + beginDate
				+ ", endDate=" + endDate + ", weekCheck=" + weekCheck + ", mondayCheck=" + mondayCheck
				+ ", tuesdayCheck=" + tuesdayCheck + ", wednesdayCheck=" + wednesdayCheck + ", thursdayCheck="
				+ thursdayCheck + ", fridayCheck=" + fridayCheck + ", saturdayCheck=" + saturdayCheck + ", sundayCheck="
				+ sundayCheck + ", dose=" + dose + ", isActive=" + isActive + "]";
	}

}
