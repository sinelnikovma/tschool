package com.tsystems.rehabilitationcenter.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "treatment_event")
public class TreatmentEvent {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "prescription_number")
	private String prescriptionNumber;

	@ManyToOne
	@JoinColumn(name = "patient_id")
	@JsonBackReference
	private Patient patient;

	@Column(name = "date_of_event")
	private LocalDateTime dateOfEvent;

	@Column(name = "status_of_event")
	private String statusOfEvent;

	@Column(name = "cancel_reason")
	private String cancelReason;

	@ManyToOne
	@JoinColumn(name = "drug_id")
	@JsonBackReference
	private Drug drug;

	@Column(name = "dose")
	private String dose;

	public TreatmentEvent() {
	}

	public TreatmentEvent(String prescriptionNumber, Patient patient, LocalDateTime dateOfEvent, String statusOfEvent,
			String cancelReason, Drug drug, String dose) {
		this.prescriptionNumber = prescriptionNumber;
		this.patient = patient;
		this.dateOfEvent = dateOfEvent;
		this.statusOfEvent = statusOfEvent;
		this.cancelReason = cancelReason;
		this.drug = drug;
		this.dose = dose;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the patient
	 */
	public Patient getPatient() {
		return patient;
	}

	/**
	 * @param patient the patient to set
	 */
	public void setPatient(Patient patient) {
		this.patient = patient;
	}

	/**
	 * @return the dateOfEvent
	 */
	public LocalDateTime getDateOfEvent() {
		return dateOfEvent;
	}

	/**
	 * @param dateOfEvent the dateOfEvent to set
	 */
	public void setDateOfEvent(LocalDateTime dateOfEvent) {
		this.dateOfEvent = dateOfEvent;
	}

	/**
	 * @return the statusOfEvent
	 */
	public String getStatusOfEvent() {
		return statusOfEvent;
	}

	/**
	 * @param statusOfEvent the statusOfEvent to set
	 */
	public void setStatusOfEvent(String statusOfEvent) {
		this.statusOfEvent = statusOfEvent;
	}

	/**
	 * @return the drug
	 */
	public Drug getDrug() {
		return drug;
	}

	/**
	 * @param drug the drug to set
	 */
	public void setDrug(Drug drug) {
		this.drug = drug;
	}

	/**
	 * @return the cancelReason
	 */
	public String getCancelReason() {
		return cancelReason;
	}

	/**
	 * @param cancelReason the cancelReason to set
	 */
	public void setCancelReason(String cancelReason) {
		this.cancelReason = cancelReason;
	}

	/**
	 * @return the dose
	 */
	public String getDose() {
		return dose;
	}

	/**
	 * @param dose the dose to set
	 */
	public void setDose(String dose) {
		this.dose = dose;
	}

	/**
	 * @return the prescriptionNumber
	 */
	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}

	/**
	 * @param prescriptionNumber the prescriptionNumber to set
	 */
	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((dose == null) ? 0 : dose.hashCode());
		result = prime * result + ((drug == null) ? 0 : drug.hashCode());
		result = prime * result + ((patient == null) ? 0 : patient.hashCode());
		result = prime * result + ((prescriptionNumber == null) ? 0 : prescriptionNumber.hashCode());
		result = prime * result + ((statusOfEvent == null) ? 0 : statusOfEvent.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TreatmentEvent other = (TreatmentEvent) obj;
		if (dose == null) {
			if (other.dose != null)
				return false;
		} else if (!dose.equals(other.dose))
			return false;
		if (drug == null) {
			if (other.drug != null)
				return false;
		} else if (!drug.equals(other.drug))
			return false;
		if (patient == null) {
			if (other.patient != null)
				return false;
		} else if (!patient.equals(other.patient))
			return false;
		if (prescriptionNumber == null) {
			if (other.prescriptionNumber != null)
				return false;
		} else if (!prescriptionNumber.equals(other.prescriptionNumber))
			return false;
		if (statusOfEvent == null) {
			if (other.statusOfEvent != null)
				return false;
		} else if (!statusOfEvent.equals(other.statusOfEvent))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "TreatmentEvent [id=" + id + ", prescriptionNumber=" + prescriptionNumber + ", patient=" + patient
				+ ", dateOfEvent=" + dateOfEvent + ", statusOfEvent=" + statusOfEvent + ", cancelReason=" + cancelReason
				+ ", drug=" + drug + ", dose=" + dose + "]";
	}

}
