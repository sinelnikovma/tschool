package com.tsystems.rehabilitationcenter.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "users")
public class SpringSecurityUsers {

	@Id
	@Column(name = "username", unique = true, nullable = false, length = 50)
	private String username;

	@Column(name = "password", nullable = false, length = 60)
	private String password;

	@Column(name = "enabled", nullable = false)
	private boolean enabled;

	@OneToMany(mappedBy = "user")
	private Set<SpringSecurityAuthority> userRole = new HashSet<>(0);

	public SpringSecurityUsers() {
		// constructor
	}

}
