package com.tsystems.rehabilitationcenter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "group_authorities")
public class SpringSecurityGroupAuthority {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "group_role_id", unique = true, nullable = false)
	private Integer groupRoleId;

	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private SpringSecurityGroup group;

	@Column(name = "authority", nullable = false, length = 50)
	private String authority;

}
