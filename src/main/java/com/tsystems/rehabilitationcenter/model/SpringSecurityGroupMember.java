package com.tsystems.rehabilitationcenter.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "group_members")
public class SpringSecurityGroupMember {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Column(name = "username", nullable = false, length = 50)
	private String username;

	@ManyToOne
	@JoinColumn(name = "group_id", nullable = false)
	private SpringSecurityGroup groupId;

	public SpringSecurityGroupMember() {
		// constructor
	}

}
