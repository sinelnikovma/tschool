package com.tsystems.rehabilitationcenter.model;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonBackReference;

@Entity
@Table(name = "physician")
public class Physician {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private Integer version;

	@Column(name = "physician_first_name")
	private String physicianFirstName;

	@Column(name = "physician_last_name")
	private String physicianLastName;

	@Column(name = "physician_code")
	private String physicianCode;

	@OneToOne
	@JoinColumn(name = "user_id", nullable = false)
	private SpringSecurityGroupMember userAccount;

	@OneToMany(mappedBy = "doctor")
	@JsonBackReference
	private Set<Patient> patientList;

	@Column(name = "active")
	private boolean isActive;

	public Physician() {
	}

	public Physician(String physicianFirstName, String physicianLastName, String physicianCode,
			SpringSecurityGroupMember userAccount, Set<Patient> patientList, boolean isActive) {
		this.physicianFirstName = physicianFirstName;
		this.physicianLastName = physicianLastName;
		this.physicianCode = physicianCode;
		this.userAccount = userAccount;
		this.patientList = patientList;
		this.isActive = isActive;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the physicianFirstName
	 */
	public String getPhysicianFirstName() {
		return physicianFirstName;
	}

	/**
	 * @param physicianFirstName the physicianFirstName to set
	 */
	public void setPhysicianFirstName(String physicianFirstName) {
		this.physicianFirstName = physicianFirstName;
	}

	/**
	 * @return the physicianLastName
	 */
	public String getPhysicianLastName() {
		return physicianLastName;
	}

	/**
	 * @param physicianLastName the physicianLastName to set
	 */
	public void setPhysicianLastName(String physicianLastName) {
		this.physicianLastName = physicianLastName;
	}

	/**
	 * @return the physicianCode
	 */
	public String getPhysicianCode() {
		return physicianCode;
	}

	/**
	 * @param physicianCode the physicianCode to set
	 */
	public void setPhysicianCode(String physicianCode) {
		this.physicianCode = physicianCode;
	}

	/**
	 * @return the userAccount
	 */
	public SpringSecurityGroupMember getUserAccount() {
		return userAccount;
	}

	/**
	 * @param userAccount the userAccount to set
	 */
	public void setUserAccount(SpringSecurityGroupMember userAccount) {
		this.userAccount = userAccount;
	}

	/**
	 * @return the patientList
	 */
	public Set<Patient> getPatientList() {
		return patientList;
	}

	/**
	 * @param patientList the patientList to set
	 */
	public void setPatientList(Set<Patient> patientList) {

		this.patientList = patientList;
	}

	/**
	 * @return the isActive
	 */
	public boolean isActive() {
		return isActive;
	}

	/**
	 * @param isActive the isActive to set
	 */
	public void setActive(boolean isActive) {
		this.isActive = isActive;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((physicianCode == null) ? 0 : physicianCode.hashCode());
		result = prime * result + ((physicianFirstName == null) ? 0 : physicianFirstName.hashCode());
		result = prime * result + ((physicianLastName == null) ? 0 : physicianLastName.hashCode());
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Physician other = (Physician) obj;
		if (physicianCode == null) {
			if (other.physicianCode != null)
				return false;
		} else if (!physicianCode.equals(other.physicianCode))
			return false;
		if (physicianFirstName == null) {
			if (other.physicianFirstName != null)
				return false;
		} else if (!physicianFirstName.equals(other.physicianFirstName))
			return false;
		if (physicianLastName == null) {
			if (other.physicianLastName != null)
				return false;
		} else if (!physicianLastName.equals(other.physicianLastName))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Physician [id=" + id + ", physicianFirstName=" + physicianFirstName + ", physicianLastName="
				+ physicianLastName + ", physicianCode=" + physicianCode + ", userAccount=" + userAccount
				+ ", isActive=" + isActive + "]";
	}

}
