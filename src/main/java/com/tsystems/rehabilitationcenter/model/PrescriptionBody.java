package com.tsystems.rehabilitationcenter.model;

import java.time.LocalTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Version;

import com.fasterxml.jackson.annotation.JsonManagedReference;

@Entity
@Table(name = "prescription_body")
public class PrescriptionBody {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;

	@Version
	private Integer version;

	@ManyToOne(fetch = FetchType.LAZY)
	@JoinColumn(name = "prescription_id")
	@JsonManagedReference
	private Prescription prescriptionId;

	@Column(name = "prescription_number")
	private String prescriptionNumber;

	@Column(name = "day")
	private String day;

	@Column(name = "reception_time")
	private LocalTime receptionTime;

	public PrescriptionBody() {
	}

	public PrescriptionBody(Prescription prescriptionId, String prescriptionNumber, String day,
			LocalTime receptionTime) {
		this.prescriptionId = prescriptionId;
		this.prescriptionNumber = prescriptionNumber;
		this.day = day;
		this.receptionTime = receptionTime;
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(Long id) {
		this.id = id;
	}

	/**
	 * @return the prescriptionId
	 */
	public Prescription getPrescriptionId() {
		return prescriptionId;
	}

	/**
	 * @param prescriptionId the prescriptionId to set
	 */
	public void setPrescriptionId(Prescription prescriptionId) {
		this.prescriptionId = prescriptionId;
	}

	/**
	 * @return the prescriptionNumber
	 */
	public String getPrescriptionNumber() {
		return prescriptionNumber;
	}

	/**
	 * @param prescriptionNumber the prescriptionNumber to set
	 */
	public void setPrescriptionNumber(String prescriptionNumber) {
		this.prescriptionNumber = prescriptionNumber;
	}

	/**
	 * @return the day
	 */
	public String getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(String day) {
		this.day = day;
	}

	/**
	 * @return the receptionTime
	 */
	public LocalTime getReceptionTime() {
		return receptionTime;
	}

	/**
	 * @param receptionTime the receptionTime to set
	 */
	public void setReceptionTime(LocalTime receptionTime) {
		this.receptionTime = receptionTime;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((day == null) ? 0 : day.hashCode());
		result = prime * result + ((prescriptionNumber == null) ? 0 : prescriptionNumber.hashCode());
		result = prime * result + ((receptionTime == null) ? 0 : receptionTime.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		PrescriptionBody other = (PrescriptionBody) obj;
		if (day == null) {
			if (other.day != null)
				return false;
		} else if (!day.equals(other.day))
			return false;
		if (prescriptionNumber == null) {
			if (other.prescriptionNumber != null)
				return false;
		} else if (!prescriptionNumber.equals(other.prescriptionNumber))
			return false;
		if (receptionTime == null) {
			if (other.receptionTime != null)
				return false;
		} else if (!receptionTime.equals(other.receptionTime))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PrescriptionBody [id=" + id + ", prescriptionNumber=" + prescriptionNumber + ", day=" + day
				+ ", receptionTime=" + receptionTime + "]";
	}

}
