package com.tsystems.rehabilitationcenter.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "persistent_logins")
public class SpringSecurityRememberMe {

	@Id
	@Column(name = "series", unique = true, nullable = false, length = 64)
	private String series;

	@Column(name = "username", nullable = false, length = 64)
	private String username;

	@Column(name = "token", nullable = false, length = 64)
	private String token;

	@Column(name = "last_used", nullable = false)
	private LocalDateTime lastUsed;

	public SpringSecurityRememberMe() {
		// constructor
	}

}
