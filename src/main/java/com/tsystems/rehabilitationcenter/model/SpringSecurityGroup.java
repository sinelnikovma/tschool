package com.tsystems.rehabilitationcenter.model;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

@Entity
@Table(name = "groups")
public class SpringSecurityGroup {

	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id", unique = true, nullable = false)
	private Long id;

	@Column(name = "group_name", nullable = false, length = 50)
	private String groupName;

	@OneToMany(mappedBy = "group")
	private Set<SpringSecurityGroupAuthority> groupRole = new HashSet<>(0);

	@OneToMany(mappedBy = "groupId")
	private Set<SpringSecurityGroupMember> groupMember = new HashSet<>(0);

	public SpringSecurityGroup() {
		// constructor
	}

}
