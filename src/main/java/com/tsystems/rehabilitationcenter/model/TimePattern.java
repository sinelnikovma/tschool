package com.tsystems.rehabilitationcenter.model;

import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Embeddable;

@Embeddable
public class TimePattern {

	@Column(name = "count_applay")
	private Integer countApplay;

	@Column(name = "everyday")
	private boolean everyday;

	@Column(name = "wholeWeek")
	private boolean wholeWeek;

	@Column(name = "sunday")
	private boolean sunday;

	@Column(name = "monday")
	private boolean monday;

	@Column(name = "tuesday")
	private boolean tuesday;

	@Column(name = "wednesday")
	private boolean wednesday;

	@Column(name = "thursday")
	private boolean thursday;

	@Column(name = "friday")
	private boolean friday;

	@Column(name = "saturday")
	private boolean saturday;

	@Column(name = "startDate")
	private LocalDateTime startDate;

	@Column(name = "finishDate")
	private LocalDateTime finishDate;

	public TimePattern() {
	}

	public TimePattern(Integer countApplay, boolean everyday, boolean wholeWeek, boolean sunday, boolean monday,
			boolean tuesday, boolean wednesday, boolean thursday, boolean friday, boolean saturday,
			LocalDateTime startDate, LocalDateTime finishDate) {
		this.countApplay = countApplay;
		this.everyday = everyday;
		this.wholeWeek = wholeWeek;
		this.sunday = sunday;
		this.monday = monday;
		this.tuesday = tuesday;
		this.wednesday = wednesday;
		this.thursday = thursday;
		this.friday = friday;
		this.saturday = saturday;
		this.startDate = startDate;
		this.finishDate = finishDate;
	}

	/**
	 * @return the countApplay
	 */
	public Integer getCountApplay() {
		return countApplay;
	}

	/**
	 * @param countApplay the countApplay to set
	 */
	public void setCountApplay(Integer countApplay) {
		this.countApplay = countApplay;
	}

	/**
	 * @return the everyday
	 */
	public boolean getEveryday() {
		return everyday;
	}

	/**
	 * @param everyday the everyday to set
	 */
	public void setEveryday(boolean everyday) {
		this.everyday = everyday;
	}

	/**
	 * @return the wholeWeek
	 */
	public boolean getWholeWeek() {
		return wholeWeek;
	}

	/**
	 * @param wholeWeek the wholeWeek to set
	 */
	public void setWholeWeek(boolean wholeWeek) {
		this.wholeWeek = wholeWeek;
	}

	/**
	 * @return the sunday
	 */
	public boolean getSunday() {
		return sunday;
	}

	/**
	 * @param sunday the sunday to set
	 */
	public void setSunday(boolean sunday) {
		this.sunday = sunday;
	}

	/**
	 * @return the monday
	 */
	public boolean getMonday() {
		return monday;
	}

	/**
	 * @param monday the monday to set
	 */
	public void setMonday(boolean monday) {
		this.monday = monday;
	}

	/**
	 * @return the tuesday
	 */
	public boolean getTuesday() {
		return tuesday;
	}

	/**
	 * @param tuesday the tuesday to set
	 */
	public void setTuesday(boolean tuesday) {
		this.tuesday = tuesday;
	}

	/**
	 * @return the wednesday
	 */
	public boolean getWednesday() {
		return wednesday;
	}

	/**
	 * @param wednesday the wednesday to set
	 */
	public void setWednesday(boolean wednesday) {
		this.wednesday = wednesday;
	}

	/**
	 * @return the thursday
	 */
	public boolean getThursday() {
		return thursday;
	}

	/**
	 * @param thursday the thursday to set
	 */
	public void setThursday(boolean thursday) {
		this.thursday = thursday;
	}

	/**
	 * @return the friday
	 */
	public boolean getFriday() {
		return friday;
	}

	/**
	 * @param friday the friday to set
	 */
	public void setFriday(boolean friday) {
		this.friday = friday;
	}

	/**
	 * @return the saturday
	 */
	public boolean getSaturday() {
		return saturday;
	}

	/**
	 * @param saturday the saturday to set
	 */
	public void setSaturday(boolean saturday) {
		this.saturday = saturday;
	}

	/**
	 * @return the startDate
	 */
	public LocalDateTime getStartDate() {
		return startDate;
	}

	/**
	 * @param startDate the startDate to set
	 */
	public void setStartDate(LocalDateTime startDate) {
		this.startDate = startDate;
	}

	/**
	 * @return the finishDate
	 */
	public LocalDateTime getFinishDate() {
		return finishDate;
	}

	/**
	 * @param finishDate the finishDate to set
	 */
	public void setFinishDate(LocalDateTime finishDate) {
		this.finishDate = finishDate;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#hashCode()
	 */
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((countApplay == null) ? 0 : countApplay.hashCode());
		result = prime * result + (everyday ? 1231 : 1237);
		result = prime * result + ((finishDate == null) ? 0 : finishDate.hashCode());
		result = prime * result + (friday ? 1231 : 1237);
		result = prime * result + (monday ? 1231 : 1237);
		result = prime * result + (saturday ? 1231 : 1237);
		result = prime * result + ((startDate == null) ? 0 : startDate.hashCode());
		result = prime * result + (sunday ? 1231 : 1237);
		result = prime * result + (thursday ? 1231 : 1237);
		result = prime * result + (tuesday ? 1231 : 1237);
		result = prime * result + (wednesday ? 1231 : 1237);
		result = prime * result + (wholeWeek ? 1231 : 1237);
		return result;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		TimePattern other = (TimePattern) obj;
		if (countApplay == null) {
			if (other.countApplay != null)
				return false;
		} else if (!countApplay.equals(other.countApplay))
			return false;
		if (everyday != other.everyday)
			return false;
		if (finishDate == null) {
			if (other.finishDate != null)
				return false;
		} else if (!finishDate.equals(other.finishDate))
			return false;
		if (friday != other.friday)
			return false;
		if (monday != other.monday)
			return false;
		if (saturday != other.saturday)
			return false;
		if (startDate == null) {
			if (other.startDate != null)
				return false;
		} else if (!startDate.equals(other.startDate))
			return false;
		if (sunday != other.sunday)
			return false;
		if (thursday != other.thursday)
			return false;
		if (tuesday != other.tuesday)
			return false;
		if (wednesday != other.wednesday)
			return false;
		if (wholeWeek != other.wholeWeek)
			return false;
		return true;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see java.lang.Object#toString()
	 */
	@Override
	public String toString() {
		return "TimePattern [countApplay=" + countApplay + ", everyday=" + everyday + ", wholeWeek=" + wholeWeek
				+ ", sunday=" + sunday + ", monday=" + monday + ", tuesday=" + tuesday + ", wednesday=" + wednesday
				+ ", thursday=" + thursday + ", friday=" + friday + ", saturday=" + saturday + ", startDate="
				+ startDate + ", finishDate=" + finishDate + "]";
	}

}
