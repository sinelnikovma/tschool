package com.tsystems.rehabilitationcenter.dao;

import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Physician;

@Repository
public class PatientDaoImpl implements PatientDao {

	@PersistenceContext
	private EntityManager em;

	public PatientDaoImpl() {
		// constructor
	}

	@Override
	public Set<Patient> getAllPatients() {
		TypedQuery<Patient> typedQuery = em.createQuery("SELECT p FROM Patient p", Patient.class);

		return typedQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Patient getPatientById(Long id) {

		return em.find(Patient.class, id);
	}

	@Override
	public Set<Patient> getPatientsByPhysician(Physician doctor) {

		String query = "SELECT p FROM Patient p where p.doctor = :doctorName";

		TypedQuery<Patient> typedQuery = em.createQuery(query, Patient.class);
		typedQuery.setParameter("doctorName", doctor);

		return typedQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public void createPatient(Patient patient) {
		em.persist(patient);
	}

	@Override
	public void updatePatient(Patient patient) {
		Patient originalPatient = em.find(Patient.class, patient.getId());
		originalPatient.setActive(true);
		originalPatient.setDiagnosis(patient.getDiagnosis());
		originalPatient.setDoctor(patient.getDoctor());
		originalPatient.setInsuranceNumber(patient.getInsuranceNumber());
		originalPatient.setStatus(patient.getStatus());

		em.flush();
	}

	@Override
	public void deletePatient(Long id) {
		em.remove(em.find(Patient.class, id));

	}

	@Override
	public void dischargePatient(Long id) {
		Patient temporaryPatient = em.find(Patient.class, id);

		temporaryPatient.setActive(false);

		em.merge(temporaryPatient);
	}

	@Override
	public Patient getPatientNyLastnameAndInsuranceNumber(Patient patient) {
		Patient originalPatient = new Patient();

		if (!patient.getInsuranceNumber().isEmpty() && !patient.getPatientLastName().isEmpty()) {
			TypedQuery<Patient> typedQuery = em.createQuery(
					"SELECT p FROM Patient p WHERE p.patientLastName = :patientLastName AND p.insuranceNumber = :insuranceNumber",
					Patient.class);
			typedQuery.setParameter("patientLastName", patient.getPatientLastName());
			typedQuery.setParameter("insuranceNumber", patient.getInsuranceNumber());

			List<Patient> resultList = typedQuery.getResultList();
			if (!resultList.isEmpty()) {
				originalPatient = resultList.get(0);
			}

		}
		return originalPatient;
	}

	@Override
	public List<Patient> getPatientByLastname(String lastname) {

		TypedQuery<Patient> query = em.createQuery("SELECT p FROM Patient p", Patient.class);

		return query.getResultList();
	}

}
