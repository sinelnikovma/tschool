package com.tsystems.rehabilitationcenter.dao;

import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.tsystems.rehabilitationcenter.model.PrescriptionBody;

@Repository
public class PrescriptionBodyDaoImpl implements PrescriptionBodyDao {

	@PersistenceContext
	private EntityManager em;

	@Override
	public Set<PrescriptionBody> getAllPrescriptionBody() {
		TypedQuery<PrescriptionBody> typedQuery = em.createQuery("SELECT b FROM PrescriptionBody b",
				PrescriptionBody.class);
		return typedQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Set<PrescriptionBody> getPrescriptionBodyByNumber(String number) {
		TypedQuery<PrescriptionBody> typedQuery = em.createQuery(
				"SELECT b FROM PrescriptionBody b WHERE b.prescriptionNumber = :number", PrescriptionBody.class);
		typedQuery.setParameter("number", number);

		return typedQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public void createPrescriptionBody(PrescriptionBody prescriptionBodies) {
		em.persist(prescriptionBodies);

	}

	@Override
	public void deletePrescriptionBody(PrescriptionBody prescriptionBody) {
		em.remove(prescriptionBody);

	}

	@Override
	public void deletePrescriptionBodyByNumber(String number) {
		Set<PrescriptionBody> bodies = getPrescriptionBodyByNumber(number);

		for (PrescriptionBody prescriptionBody : bodies) {
			deletePrescriptionBody(prescriptionBody);
		}

	}

}
