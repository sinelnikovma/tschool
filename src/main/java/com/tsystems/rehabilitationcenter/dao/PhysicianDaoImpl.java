package com.tsystems.rehabilitationcenter.dao;

import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.tsystems.rehabilitationcenter.model.Physician;

@Repository
public class PhysicianDaoImpl implements PhysicianDao {

	private static final Logger logger = Logger.getLogger(PhysicianDaoImpl.class.getName());

	@PersistenceContext
	private EntityManager entityManager;

	public PhysicianDaoImpl() {
		// constructor
	}

	@Override
	public Set<Physician> getAllPhysician() {

		TypedQuery<Physician> query = entityManager.createQuery("SELECT p FROM Physician p", Physician.class);
		return query.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Physician getPhysycianById(Long id) {
		logger.log(Level.SEVERE, "method getPhysycianById(Long id) didn't release");
		return null;
	}

	@Override
	public void createPhysician(PhysicianDao doctor) {
		logger.log(Level.SEVERE, "method createPhysician(PhysicianDao doctor) didn't release");

	}

	@Override
	public void updatePhysician(PhysicianDao doctor) {
		logger.log(Level.SEVERE, "method updatePhysician(PhysicianDao doctor) didn't release");

	}

	@Override
	public void deletePhysician(Long id) {
		logger.log(Level.SEVERE, "method deletePhysician(Long id) didn't release");

	}

	@Override
	public Physician getPhysicianBySecureId(String principalName) {
		TypedQuery<Physician> query = entityManager.createQuery(
				"SELECT p FROM Physician p INNER JOIN p.userAccount gm WHERE gm.username = :principalName",
				Physician.class);
		query.setParameter("principalName", principalName);
		return query.getSingleResult();
	}
}
