package com.tsystems.rehabilitationcenter.dao;

import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.tsystems.rehabilitationcenter.model.Drug;

@Repository
public class DrugDaoImpl implements DrugDao {

	private static Logger logger = Logger.getLogger(DrugDaoImpl.class.getName());

	@PersistenceContext
	private EntityManager em;

	@Override
	public Set<Drug> getAllDrugs() {

		TypedQuery<Drug> typedQuery = em.createQuery("SELECT d FROM Drug d", Drug.class);
		return typedQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Set<Drug> getDrugsByType(String type) {
		logger.log(Level.SEVERE, "method getDrugsByType(String type) didn't release");
		return Collections.emptySet();
	}

	@Override
	public Drug getDrugById(Long id) {

		return em.find(Drug.class, id);
	}

	@Override
	public void createDrug(Drug drug) {
		logger.log(Level.SEVERE, "method createDrug(Drug drug) didn't release");

	}

	@Override
	public void updateDrug(Drug drug) {
		logger.log(Level.SEVERE, "method updateDrug(Drug drug) didn't release");

	}

	@Override
	public void deleteDrug(Long id) {
		logger.log(Level.SEVERE, "method deleteDrug(Long id) didn't release");

	}

}
