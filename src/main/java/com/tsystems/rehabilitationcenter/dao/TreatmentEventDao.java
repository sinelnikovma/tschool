package com.tsystems.rehabilitationcenter.dao;

import java.util.List;

import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.TreatmentEvent;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

public interface TreatmentEventDao {

	public List<TreatmentEvent> getAllTreatmentEvent();

	public List<TreatmentEvent> getTreatmentEventByFilter(TreatmentEventObjectFilter treatmentEventFilter);

	public void closeTreatmentEventByPatient(Patient patient, String eventStatus);

	public void deleteTreatmentEventByPatient(Patient patient);

	public void changeStatus(TreatmentEvent treatmentEvent);

	public TreatmentEvent getTreatmentEventById(Long id);

	public List<TreatmentEvent> getTreatmentEventByPrescriptionNumber(String prescriptionNumber);

	public void deleteTreatmentEventByPrescriptionNumber(String prescriptionNumber);

	public void deleteTreatmentEvent(TreatmentEvent event);

	public void createTreatmentEvent(TreatmentEvent event);
}
