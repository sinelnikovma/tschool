package com.tsystems.rehabilitationcenter.dao;

import java.util.Collections;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;

import org.springframework.stereotype.Repository;

import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Prescription;

@Repository
public class PrescriptionDaoImpl implements PrescriptionDao {

	private static final Logger logger = Logger.getLogger(PrescriptionDaoImpl.class.getName());

	@PersistenceContext
	private EntityManager em;

	@Override
	public Set<Prescription> getAllPrescriptions() {

		TypedQuery<Prescription> createQuery = em.createQuery("SELECT p FROM Prescription p", Prescription.class);
		return createQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public Set<Prescription> getPrescriptionsByPhysicianId(Long id) {
		logger.log(Level.SEVERE, "method getPrescriptionsByPhysicianId(Long id) didn't release");
		return Collections.emptySet();
	}

	@Override
	public Prescription getPrescriptionById(Long id) {

		return em.find(Prescription.class, id);
	}

	@Override
	public Set<Prescription> getPrescriptionsByPatient(Long id) {
		TypedQuery<Prescription> typedQuery = em.createQuery("SELECT p FROM Prescription p WHERE p.patient.id = :id",
				Prescription.class);
		typedQuery.setParameter("id", id);

		return typedQuery.getResultStream().collect(Collectors.toSet());
	}

	@Override
	public void createPrescrition(Prescription prescription) {
		em.persist(prescription);
	}

	@Override
	public void updatePrescription(Prescription prescription) {
		Prescription originalPrescription = em.find(Prescription.class, prescription.getId());
		originalPrescription.setPatient(prescription.getPatient());
		originalPrescription.setDrugType(prescription.getDrugType());
		originalPrescription.setDose(prescription.getDose());
		originalPrescription.setBeginDate(prescription.getBeginDate());
		originalPrescription.setEndDate(prescription.getEndDate());
		originalPrescription.setPrescriptionBody(prescription.getPrescriptionBody());
		originalPrescription.setWeekCheck(prescription.isWeekCheck());
		originalPrescription.setMondayCheck(prescription.isMondayCheck());
		originalPrescription.setTuesdayCheck(prescription.isTuesdayCheck());
		originalPrescription.setWednesdayCheck(prescription.isWednesdayCheck());
		originalPrescription.setThursdayCheck(prescription.isThursdayCheck());
		originalPrescription.setFridayCheck(prescription.isFridayCheck());
		originalPrescription.setSaturdayCheck(prescription.isSaturdayCheck());
		originalPrescription.setSundayCheck(prescription.isSundayCheck());

		em.flush();

	}

	@Override
	public void deletePrescription(Prescription prescription) {
		Prescription deletePrescription = em.find(Prescription.class, prescription.getId());
		em.remove(deletePrescription);

	}

	@Override
	public void finishPrescription(Patient patient) {
		logger.log(Level.SEVERE, "method finishPrescription(Patient patient) didn't release");

	}

	@Override
	public Prescription getPrescriptionByNumber(String number) {
		TypedQuery<Prescription> typedQuery = em
				.createQuery("SELECT p FROM Prescription p WHERE p.prescriptionNumber = :number", Prescription.class);
		typedQuery.setParameter("number", number);
		Prescription singleResult = null;
		try {
			singleResult = typedQuery.getSingleResult();
		} catch (NoResultException e) {
			return null;
		}

		return singleResult;
	}

}
