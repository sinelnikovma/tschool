package com.tsystems.rehabilitationcenter.dao;

import java.util.Set;

import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Prescription;

public interface PrescriptionDao {

	/**
	 * Get all prescriptions
	 * 
	 * @return
	 */
	public Set<Prescription> getAllPrescriptions();

	/**
	 * Get prescription by physician identificator
	 * 
	 * @param id
	 * @return
	 */
	public Set<Prescription> getPrescriptionsByPhysicianId(Long id);

	public Prescription getPrescriptionById(Long id);

	public Prescription getPrescriptionByNumber(String number);

	public Set<Prescription> getPrescriptionsByPatient(Long id);

	public void createPrescrition(Prescription prescription);

	public void updatePrescription(Prescription prescription);

	public void deletePrescription(Prescription prescription);

	/**
	 * FInish all prescription of patient
	 * 
	 * @param prescription
	 */
	public void finishPrescription(Patient patient);

}
