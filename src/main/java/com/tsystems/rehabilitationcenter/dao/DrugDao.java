package com.tsystems.rehabilitationcenter.dao;

import java.util.Set;

import com.tsystems.rehabilitationcenter.model.Drug;

public interface DrugDao {

	public Set<Drug> getAllDrugs();

	public Set<Drug> getDrugsByType(String type);

	public Drug getDrugById(Long id);

	public void createDrug(Drug drug);

	public void updateDrug(Drug drug);

	public void deleteDrug(Long id);

}
