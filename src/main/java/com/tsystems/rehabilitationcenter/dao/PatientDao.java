package com.tsystems.rehabilitationcenter.dao;

import java.util.List;
import java.util.Set;

import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.Physician;

public interface PatientDao {

	public Set<Patient> getAllPatients();

	public Patient getPatientById(Long id);

	public Patient getPatientNyLastnameAndInsuranceNumber(Patient patient);

	public Set<Patient> getPatientsByPhysician(Physician doctor);

	public void createPatient(Patient patient);

	public void updatePatient(Patient patient);

	public void deletePatient(Long id);

	public void dischargePatient(Long id);

	public List<Patient> getPatientByLastname(String lastname);
}
