package com.tsystems.rehabilitationcenter.dao;

import java.util.Set;

import com.tsystems.rehabilitationcenter.model.PrescriptionBody;

public interface PrescriptionBodyDao {

	public Set<PrescriptionBody> getAllPrescriptionBody();

	public Set<PrescriptionBody> getPrescriptionBodyByNumber(String number);

	public void createPrescriptionBody(PrescriptionBody prescriptionBodies);

	public void deletePrescriptionBody(PrescriptionBody prescriptionBody);

	public void deletePrescriptionBodyByNumber(String number);

}
