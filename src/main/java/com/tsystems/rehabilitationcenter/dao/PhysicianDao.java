package com.tsystems.rehabilitationcenter.dao;

import java.util.Set;

import com.tsystems.rehabilitationcenter.model.Physician;

public interface PhysicianDao {

	public Set<Physician> getAllPhysician();

	public Physician getPhysycianById(Long id);

	public void createPhysician(PhysicianDao doctor);

	public void updatePhysician(PhysicianDao doctor);

	public void deletePhysician(Long id);

	public Physician getPhysicianBySecureId(String name);
}
