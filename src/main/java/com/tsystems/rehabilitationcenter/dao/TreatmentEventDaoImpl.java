package com.tsystems.rehabilitationcenter.dao;

import java.sql.Date;
import java.sql.Time;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Expression;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.springframework.stereotype.Repository;

import com.tsystems.rehabilitationcenter.model.Patient;
import com.tsystems.rehabilitationcenter.model.TreatmentEvent;
import com.tsystems.rehabilitationcenter.view.TreatmentEventObjectFilter;

@Repository
public class TreatmentEventDaoImpl implements TreatmentEventDao {

	private static final Logger logger = Logger.getLogger(TreatmentEventDaoImpl.class.getName());

	@PersistenceContext
	private EntityManager em;

	@Override
	public List<TreatmentEvent> getAllTreatmentEvent() {
		TypedQuery<TreatmentEvent> query = em.createQuery("SELECT e FROM TreatmentEvent e", TreatmentEvent.class);

		return query.getResultList();
	}

	@Override
	public void closeTreatmentEventByPatient(Patient patient, String eventStatus) {
		logger.log(Level.SEVERE,
				"method closeTreatmentEventByPatient(Patient patient, String eventStatus) didn't release");

	}

	@Override
	public void deleteTreatmentEventByPatient(Patient patient) {
		TypedQuery<TreatmentEvent> query = em.createQuery("SELECT e FROM TreatmentEvent e WHERE e.patient = :patient",
				TreatmentEvent.class);
		query.setParameter("patient", patient);
		List<TreatmentEvent> resultList = query.getResultList();

		for (TreatmentEvent treatmentEvent : resultList) {
			em.remove(treatmentEvent);
		}
	}

	@Override
	public List<TreatmentEvent> getTreatmentEventByFilter(TreatmentEventObjectFilter treatmentEventFilter) {
		if (treatmentEventFilter.getPatientFilter().getId() == null && treatmentEventFilter.getDateFilter().isEmpty()
				&& treatmentEventFilter.getDrugFilter().getId() == null
				&& treatmentEventFilter.getTimeFilter().isEmpty()) {
			return getAllTreatmentEvent();
		}

		CriteriaQuery<TreatmentEvent> criteriaQuery = builderQuery(treatmentEventFilter);

		TypedQuery<TreatmentEvent> typedQuery = em.createQuery(criteriaQuery);
		return typedQuery.getResultList();
	}

	private CriteriaQuery<TreatmentEvent> builderQuery(TreatmentEventObjectFilter treatmentEventFilter) {
		CriteriaBuilder criteriaBuilder = em.getCriteriaBuilder();
		CriteriaQuery<TreatmentEvent> criteriaQuery = criteriaBuilder.createQuery(TreatmentEvent.class);

		Root<TreatmentEvent> root = criteriaQuery.from(TreatmentEvent.class);
		criteriaQuery.select(root);

		List<Predicate> predicates = new ArrayList<>();

		if (treatmentEventFilter.getPatientFilter().getId() != null) {
			predicates.add(criteriaBuilder.equal(root.get("patient"), treatmentEventFilter.getPatientFilter().getId()));
		}

		if (treatmentEventFilter.getDrugFilter().getId() != null) {
			predicates.add(criteriaBuilder.equal(root.get("drug"), treatmentEventFilter.getDrugFilter().getId()));
		}

		if (!treatmentEventFilter.getDateFilter().isEmpty()) {
			Path<Date> localDate = root.get("dateOfEvent");
			String dateFilter = treatmentEventFilter.getDateFilter();
			Expression<Integer> yearFunction = criteriaBuilder.function("year", Integer.class, localDate);
			Expression<Integer> monthFunction = criteriaBuilder.function("month", Integer.class, localDate);
			Expression<Integer> dayFunction = criteriaBuilder.function("day", Integer.class, localDate);
			Integer localYear = Integer.parseInt(dateFilter.substring(0, 4));
			Integer localMonth = Integer.parseInt(dateFilter.substring(5, 7));
			Integer localDay = Integer.parseInt(dateFilter.substring(8, 10));
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(yearFunction, localYear),
					criteriaBuilder.equal(monthFunction, localMonth), criteriaBuilder.equal(dayFunction, localDay)));
		}

		if (!treatmentEventFilter.getTimeFilter().isEmpty()) {
			Path<Time> localDate = root.get("dateOfEvent");
			String dateFilter = treatmentEventFilter.getTimeFilter();
			Expression<Integer> hourFunction = criteriaBuilder.function("hour", Integer.class, localDate);
			Expression<Integer> secondFunction = criteriaBuilder.function("second", Integer.class, localDate);

			Integer localHour = Integer.parseInt(dateFilter.substring(0, 2));
			Integer localSecond = Integer.parseInt(dateFilter.substring(3, 5));
			predicates.add(criteriaBuilder.and(criteriaBuilder.equal(hourFunction, localHour),
					criteriaBuilder.equal(secondFunction, localSecond)));
		}

		criteriaQuery.where(predicates.toArray(new Predicate[] {}));
		return criteriaQuery;
	}

	@Override
	public void changeStatus(TreatmentEvent treatmentEvent) {
		TreatmentEvent originalTreatmentEvent = em.find(TreatmentEvent.class, treatmentEvent.getId());
		originalTreatmentEvent.setStatusOfEvent(treatmentEvent.getStatusOfEvent());
		originalTreatmentEvent.setCancelReason(treatmentEvent.getCancelReason());

		em.flush();

	}

	@Override
	public TreatmentEvent getTreatmentEventById(Long id) {

		return em.find(TreatmentEvent.class, id);
	}

	@Override
	public List<TreatmentEvent> getTreatmentEventByPrescriptionNumber(String prescriptionNumber) {

		TypedQuery<TreatmentEvent> typedQuery = em.createQuery(
				"SELECT e FROM TreatmentEvent e WHERE e.prescriptionNumber = :prescriptionNumber",
				TreatmentEvent.class);
		typedQuery.setParameter("prescriptionNumber", prescriptionNumber);

		return typedQuery.getResultList();
	}

	@Override
	public void deleteTreatmentEventByPrescriptionNumber(String prescriptionNumber) {
		List<TreatmentEvent> treatmentList = getTreatmentEventByPrescriptionNumber(prescriptionNumber);

		if (!treatmentList.isEmpty()) {
			for (TreatmentEvent treatmentEvent : treatmentList) {
				deleteTreatmentEvent(treatmentEvent);
			}
		}

	}

	@Override
	public void deleteTreatmentEvent(TreatmentEvent event) {
		TreatmentEvent deleteTreatmentEvent = em.find(TreatmentEvent.class, event.getId());
		em.remove(deleteTreatmentEvent);
	}

	@Override
	public void createTreatmentEvent(TreatmentEvent event) {
		em.persist(event);
	}

}
