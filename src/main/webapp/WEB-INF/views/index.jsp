<%@page import="org.springframework.web.servlet.handler.UserRoleAuthorizationInterceptor"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="core"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Rehabilitation Center</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
</head>
<body>
	<div class="wrapper">
		<security:authorize access="hasRole('ROLE_DOCTOR')" var="isDoctor" />
		<security:authorize access="hasRole('ROLE_NURSE')" var="isNurse" />
		<div class="container">
			<%@include file="fragment/header.html"%>

			<nav>
				<ul class="topmenu">

					<core:if test="${isDoctor }">
						<li><a class="">Physician menu</a>
							<ul class="submenu">
								<li><a class="buttons" href="${pageContext.request.contextPath}/patientaction/showAllPatient">Patients</a></li>
								<li><a class="buttons" href="${pageContext.request.contextPath}/patientaction/showPrescriptionList">Prescriptions and drugs</a></li>
							</ul></li>
					</core:if>
					<core:if test="${isNurse }">
						<li><a class="">Nurse menu</a>
							<ul class="submenu">
								<li><a class="buttons" href="${pageContext.request.contextPath}/treatmentEvent/treatmentEvent">Show all treatment events</a></li>
							</ul></li>
					</core:if>
					<li><a class="" href='${pageContext.request.contextPath}/logout'>Logout</a></li>
				</ul>
			</nav>
			<img alt="Cool picture" src="${pageContext.request.contextPath}/resources/images/main.jpg" height="700">
		</div>
		<%@include file="fragment/footer.html"%>
	</div>
</body>
</html>