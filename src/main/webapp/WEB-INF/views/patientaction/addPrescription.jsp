<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Prescription edit</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/addPrescription.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/addPrescription.js"></script>
</head>

<body>
	<div class="wrapper">
		<div class="container">
			<%@include file="../fragment/header.html"%>
			<div class="main">
				<form:form id="id_prescription" action="addPrescription" method="post" modelAttribute="prescriptionDto">

					<div class="common_data form-inline m-2">

						<form:hidden path="id" />

						<form:label path="prescriptionNumber" placeholder="0001" class="text-info mr-1">Prescription number:  		
						</form:label>
						<form:input path="prescriptionNumber" type="text" value="${prescriptionDto.prescriptionNumber}" class="form-control mr-1 mb-1" />
						<form:errors path="prescriptionNumber" cssClass="error bg-danger text-white form-control mr-1 mb-1"/>


						<form:label path="patient.id" class="text-info mr-1">Pacient: 
						<form:select name="patient" path="patient.id" value="${prescriptionDto}" class="form-control mr-1 mb-1">
								<c:forEach var="patient" items="${patientList}">
									<form:option value="${patient.id}">
										<c:out value='${patient.patientFirstName}  ${patient.patientLastName}' />
									</form:option>
								</c:forEach>
							</form:select>
						</form:label>


						<form:label path="drugType.id" class="text-info mr-1">Drug: 
						<form:select name="drug" path="drugType.id" class="form-control mr-1 mb-1">
								<c:forEach var="drug" items="${allDrug}">
									<form:option value="${drug.id}">
										<c:out value="${drug.drugName}" />
									</form:option>
								</c:forEach>
							</form:select>
						</form:label>

						<form:label path="dose" class="text-info  mr-1">Dose: 
						<form:input path="dose" type="text" name="dose" placeholder="dose" value="${prescriptionDto.dose}" class="form-control mr-1 mb-1" />
						</form:label>

						<form:label path="beginDate" class="text-info mr-1">Begin date: 
						<form:input path="beginDate" type="date" name="begin_date" class="form-control mr-1 mb-1 mt-1" />
							<form:errors path="beginDate" cssClass="error bg-danger text-white form-control mr-1 mb-1 mt-1" />
						</form:label>

						<form:label path="endDate" class="text-info mr-1">End date: 
						<form:input path="endDate" type="date" name="end_date" class="form-control mr-1 mb-1 mt-1" />
							<form:errors path="endDate" cssClass="error bg-danger text-white form-control mr-1 mb-1 mt-1" />
						</form:label>

						<form:button type="submit" form="id_prescription" class="btn btn-info mr-1 mt-1">Save</form:button>
						<button type="button" onclick='window.location.href="showPrescriptionList"' class="btn btn-info mt-1">Back</button>

					</div>
					<div class="common_time">
						<div class="week_container">

							<div class="week_head thead">
								<form:label path="weekdays">Whole week<form:checkbox path="weekdays" value="week" />
								</form:label>
							</div>
							<div class="week_body">
								<div class="sell_time_head">
									<button id="add_week" type="button" class="js_add" onclick='addTimeBlock("week")'>Add time</button>
								</div>
								<div class="week_times">
									<div class="example_week_time week_time none_display datetime">
										<form:input path="weekTime" type="time" class="week_time sell_time" value="09:00" disabled="true" />
										<button type="button" class="js_remove">X</button>
									</div>

									<c:forEach var="time" items="${prescriptionDto.weekTime}" varStatus="vs">
										<div class="week_time datetime">
											<form:input path="weekTime" type="time" value="${time}" class="week_time sell_time" />
											<button type="button" class="js_remove">X</button>
										</div>
									</c:forEach>
								</div>
							</div>
						</div>
						<div class="days_container">
							<table class="times_table border_board">
								<thead>
									<tr>
										<th class="thead"><form:label path="weekdays">Monday<form:checkbox path="weekdays" value="monday" />
											</form:label></th>
										<th class="thead"><form:label path="weekdays">Tuesday<form:checkbox path="weekdays" value="tuesday" />
											</form:label></th>
										<th class="thead"><form:label path="weekdays">Wednesday<form:checkbox path="weekdays" value="wednesday" />
											</form:label></th>
										<th class="thead"><form:label path="weekdays">Thursday<form:checkbox path="weekdays" value="thursday" />
											</form:label></th>
										<th class="thead"><form:label path="weekdays">Friday<form:checkbox path="weekdays" value="friday" />
											</form:label></th>
										<th class="thead"><form:label path="weekdays">Saturday<form:checkbox path="weekdays" value="saturday" />
											</form:label></th>
										<th class="thead"><form:label path="weekdays">Sunday<form:checkbox path="weekdays" value="sunday" />
											</form:label></th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_monday" type="button" class="js_add" onclick='addTimeBlock("monday")'>Add time</button>
											</div>
											<div class="monday_times">
												<div class="example_monday_time monday_time none_display datetime">
													<form:input path="mondayTime" type="time" class="monday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.mondayTime}" varStatus="vs">
													<div class="monday_time datetime">
														<form:input path="mondayTime" type="time" value="${time}" class="monday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>
											</div>
										</td>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_tuesday" type="button" class="js_add" onclick='addTimeBlock("tuesday")'>Add time</button>
											</div>
											<div class="tuesday_times">
												<div class="example_tuesday_time tuesday_time none_display datetime">
													<form:input path="tuesdayTime" type="time" class="tuesday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.tuesdayTime}" varStatus="vs">
													<div class="tuesday_time datetime">
														<form:input path="tuesdayTime" type="time" value="${time}" class="tuesday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>

											</div>
										</td>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_wednesday" type="button" class="js_add" onclick='addTimeBlock("wednesday")'>Add time</button>
											</div>
											<div class="wednesday_times">
												<div class="example_wednesday_time wednesday_time none_display datetime">
													<form:input path="wednesdayTime" type="time" class="wednesday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.wednesdayTime}" varStatus="vs">
													<div class="wednesday_time datetime">
														<form:input path="wednesdayTime" type="time" value="${time}" class="wednesday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>
											</div>
										</td>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_thursday" type="button" class="js_add" onclick='addTimeBlock("thursday")'>Add time</button>
											</div>
											<div class="thursday_times">
												<div class="example_thursday_time thursday_time none_display datetime">
													<form:input path="thursdayTime" type="time" class="thursday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.thursdayTime}" varStatus="vs">
													<div class="thursday_time datetime">
														<form:input path="thursdayTime" type="time" value="${time}" class="thursday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>
											</div>
										</td>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_friday" type="button" class="js_add" onclick='addTimeBlock("friday")'>Add time</button>
											</div>
											<div class="friday_times">
												<div class="example_friday_time friday_time none_display datetime">
													<form:input path="fridayTime" type="time" class="friday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.fridayTime}" varStatus="vs">
													<div class="friday_time datetime">
														<form:input path="fridayTime" name="time" type="time" value="${time}" class="friday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>
											</div>
										</td>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_saturday" type="button" class="js_add" onclick='addTimeBlock("saturday")'>Add time</button>
											</div>
											<div class="saturday_times">
												<div class="example_saturday_time saturday_time none_display datetime">
													<form:input path="saturdayTime" type="time" class="saturday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.saturdayTime}" varStatus="vs">
													<div class="saturday_time datetime">
														<form:input path="saturdayTime" name="time" type="time" value="${time}" class="saturday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>
											</div>
										</td>
										<td class="border_sell_right">
											<div class="sell_time_head">
												<button id="add_sunday" type="button" class="js_add" onclick='addTimeBlock("sunday")'>Add time</button>
											</div>
											<div class="sunday_times">
												<div class="example_sunday_time sunday_time none_display datetime">
													<form:input path="sundayTime" type="time" class="sunday_time sell_time" value="00:00" disabled="true" />
													<button type="button" class="js_remove">X</button>
												</div>

												<c:forEach var="time" items="${prescriptionDto.sundayTime}" varStatus="vs">
													<div class="sunday_time datetime">
														<form:input path="sundayTime" name="time" type="time" value="${time}" class="sunday_time sell_time" />
														<button type="button" class="js_remove">X</button>
													</div>
												</c:forEach>
											</div>
										</td>
									</tr>
								</tbody>
							</table>
						</div>
					</div>
				</form:form>
			</div>
		</div>
		<%@include file="../fragment/footer.html"%>

	</div>
</body>
</html>