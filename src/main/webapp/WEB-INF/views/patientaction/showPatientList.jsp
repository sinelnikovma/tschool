<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Patient list</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/showPatientList.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/showPatient.js"></script>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<%@include file="../fragment/header.html"%>
			<button class="btn btn-info m-2" onclick="editUser()">Add|Change patient</button>
			<button class="btn btn-info m-2" onclick="dischargeUser()">Discharge patient</button>
			<button type="button" onclick='window.location.href="${pageContext.request.contextPath}"' class="btn btn-info m-2">Back</button>
			<table id="tab">
				<thead>
					<tr>
						<th>id</th>
						<th>Last Name</th>
						<th>First Name</th>
						<th>Insurance Number</th>
						<th>Doctor</th>
						<th>Status</th>
						<th>Diagnosis</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="patient" items="${patientList}">
						<tr>
							<td>${patient.id}</td>
							<td>${patient.patientLastName}</td>
							<td>${patient.patientFirstName}</td>
							<td>${patient.insuranceNumber}</td>
							<td>${patient.doctor.physicianLastName} ${patient.doctor.physicianFirstName}</td>
							<td>${patient.status}</td>
							<td>${patient.diagnosis}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<%@include file="../fragment/footer.html"%>
	</div>
</body>
</html>