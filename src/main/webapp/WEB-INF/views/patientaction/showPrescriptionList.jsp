<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Prescription list</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/showPrescriptionList.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/showPrescriptionList.js"></script>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<%@include file="../fragment/header.html"%>

			<div class="float-left mr-1">
				<table id="patient_table">
					<thead>
						<tr>
							<th class="none_display">id</th>
							<th>Patient</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td class="none_display">0</td>
							<td>All patients</td>
						</tr>
						<c:forEach var="patient" items="${patientList}">
							<tr>
								<td class="none_display">${patient.id}</td>
								<td>${patient.patientFirstName}  ${patient.patientLastName}</td>
							</tr>
						</c:forEach>
					</tbody>
				</table>
			</div>
			<div class="prescription_list m-1">
				<table id="prescription_table">
					<tr>
						<th class="none_display">Prescription number</th>
						<th>Prescription</th>
					</tr>
					<c:forEach var="prescription" items="${prescriptionList}">
						<tr>
							<td class="none_display">${prescription.prescriptionNumber}</td>
							<td>${prescription.prescriptionNumber}  ${prescription.drugType.drugName}  ${prescription.dose}</td>
						</tr>
					</c:forEach>
				</table>
			</div>
			<button id="add_prescription" class="btn btn-info d-block m-1" onclick="editPrescription()">Add | Change prescription</button>
			<button id="cansel_prescription" class="btn btn-info d-block m-1" onclick="cancelPrescription()">Cancel prescription</button>
			<button type="button" onclick='window.location.href="${pageContext.request.contextPath}"' class="btn btn-info d-block m-1">Back</button>
		</div>
		<%@include file="../fragment/footer.html"%>
	</div>
</body>
</html>