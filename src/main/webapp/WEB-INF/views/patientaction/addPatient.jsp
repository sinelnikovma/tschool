<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Patient edit</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/addPatient.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<%@include file="../fragment/header.html"%>

			<form:form action="addPatient" method="post" modelAttribute="patientDto">
				<!-- enctype="multipart/form-data"> -->
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

				<table class="border mt-1">

					<tr>
						<th><form:label path="patientFirstName">Name</form:label></th>
						<td><form:input type="text" name="patientFirstName" path="patientFirstName" value="${patient.patientFirstName}" class="form-control" /></td>
					</tr>



					<tr>
						<th><form:label path="patientLastName">Surename</form:label></th>
						<td><form:input type="text" name="patientLastName" path="patientLastName" value="${patient.patientLastName}" class="form-control" /> </td>
						<td><form:errors
								path="patientLastName" cssClass="error bg-danger text-white form-control" /></td>
					</tr>



					<tr>
						<th><form:label path="diagnosis">Diagnosis</form:label></th>
						<td><form:input type="text" name="diagnosis" path="diagnosis" value="${patient.diagnosis}" class="form-control" /></td>
					</tr>



					<tr>
						<th><form:label path="insuranceNumber">Insurance number</form:label></th>
						<td><form:input type="text" name="insuranceNumber" path="insuranceNumber" value="${patient.insuranceNumber}" class="form-control" /> </td>
						<td><form:errors
								path="insuranceNumber" cssClass="error bg-danger text-white form-control" /></td>
					</tr>



					<tr>
						<th><form:label path="">Physician</form:label></th>
						<td><form:input type="text" path="" value="${patient.doctor.physicianLastName} ${patient.doctor.physicianFirstName}" class="form-control" /></td>
					</tr>



					<tr>
						<th><form:label path="doctor.id">Physician list</form:label></th>
						<td><form:select class="form-control" path="doctor.id" name="doctor">
								<c:forEach var="doctor" items="${doctorList}">
									<form:option value="${doctor.id}">
										<c:out value="${doctor.physicianFirstName} ${doctor.physicianLastName}" />
									</form:option>
								</c:forEach>
							</form:select></td>
					</tr>


				</table>
				<div>
					<form:input path="id" type="hidden" value="${patient.id}" />
				</div>

				<form:button type="submit" class="btn btn-info mt-1">Save</form:button>
				<button type="button" onclick='window.location.href="showAllPatient"' class="btn btn-info mt-1">Back</button>
			</form:form>
		</div>
		<%@include file="../fragment/footer.html"%>
	</div>
</body>
</html>