<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Treatment events</title>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/treatmentEvent.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/ajaxUtils.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/resources/script/treatmentEvent.js"></script>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<%@include file="../fragment/header.html"%>
			<form:form id="filter_block" action="treatmentEvent" method="post" modelAttribute="eventFilter" class="form-inline ">
				<form:select name="patient" class="patient_list form-control mr-2" path="patientFilter.id" value="${eventFilter}">
					<form:option value=""></form:option>
					<c:forEach var="patient" items="${allPatient}">
						<form:option value="${patient.id}">
							<c:out value='${patient.patientFirstName}  ${patient.patientLastName}' />
						</form:option>
					</c:forEach>
				</form:select>
				<form:select name="drug" class="drug_list form-control mr-2" path="drugFilter.id" value="${eventFilter}">
					<form:option value=""></form:option>
					<c:forEach var="drug" items="${allDrug}">
						<form:option value="${drug.id}">
							<c:out value="${drug.drugName}" />
						</form:option>
					</c:forEach>
				</form:select>
				<form:input path="dateFilter" type="date" placeholder="Date Time" name="date" class="form-control mr-2" />
				<form:input path="timeFilter" type="time" name="time" class="form-control mr-2" />
				<form:button id="filter" type="submit" class="btn btn-info mr-2">Filter</form:button>
				<button id="change_status" type="button" onclick="changeStatus()" class="btn btn-info  mr-2">Change status</button>
				<button type="button" onclick='window.location.href="${pageContext.request.contextPath}"' class="btn btn-info">Back</button>
			</form:form>

			<table id="tab">
				<thead>
					<tr>
						<th>id</th>
						<th>Patient</th>
						<th>Drug</th>
						<th>Dose</th>
						<th>Date</th>
						<th>Time</th>
						<th>Status</th>
						<th>Reason of Cancel</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="treatmentEvent" items="${treatmentEventList}">
						<tr>
							<td>${treatmentEvent.id}</td>
							<td>${treatmentEvent.patient.patientFirstName}${treatmentEvent.patient.patientLastName}</td>
							<td>${treatmentEvent.drug.drugName}</td>
							<td>${treatmentEvent.dose}</td>
							<td>${treatmentEvent.date}</td>
							<td>${treatmentEvent.time}</td>
							<td>${treatmentEvent.statusOfEvent}</td>
							<td>${treatmentEvent.cancelReason}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>
		<%@include file="../fragment/footer.html"%>
	</div>
</body>
</html>