<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/changeStatus.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/css/common.css">
<title>Insert title here</title>
</head>
<body>
	<div class="wrapper">
		<div class="container">
			<%@include file="../fragment/header.html"%>
			<form:form action="changeStatus" method="post" modelAttribute="treatmentEventDto">
				<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />

				<table class="border mt-1">
					<tr>
						<th><form:label path="" >id</form:label></th>
						<td><form:input type="text" name="id" value="${treatmentEventDto.id}" path="" disabled="true"  class="form-control"/></td>
					</tr>
					<tr>
						<th><form:label path="">Patient</form:label></th>
						<td><form:input type="text" name="patient" value="${treatmentEventDto.patient}" path="" disabled="true" class="form-control"/></td>
					</tr>
					<tr>
						<th><form:label path="">Drug</form:label></th>
						<td><form:input type="text" name="drug" value="${treatmentEventDto.drug.drugName}" path="" disabled="true" class="form-control"/></td>
					</tr>
					<tr>
						<th><form:label path="">Dose</form:label></th>
						<td><form:input type="text" name="dose" value="${treatmentEventDto.dose}" path="" disabled="true" class="form-control"/></td>
					</tr>
					<tr>
						<th><form:label path="">Date</form:label></th>
						<td><form:input type="text" name="date" value="${treatmentEventDto.dateOfEvent}" path="" disabled="true" class="form-control"/></td>
					</tr>
					<tr>
						<th><form:label path="">Time</form:label></th>
						<td><form:input type="text" name="time" value="${treatmentEventDto.dateOfEvent}" path="" disabled="true" class="form-control"/></td>
					</tr>
					<tr>
						<th><form:label path="statusOfEvent">Status</form:label></th>
						<td>
							<!-- 			<form:input type="text" name="status" value="${treatmentEventDto.statusOfEvent}" path="statusOfEvent" /> --> <form:select name="status"
								class="status_class form-control" path="statusOfEvent">
								<form:option value="planned">planned</form:option>
								<form:option value="writeout">writeout</form:option>
								<form:option value="canceled">canceled</form:option>
							</form:select>
						</td>
					</tr>
					<tr>
						<th><form:label path="cancelReason">Reason</form:label></th>
						<td><form:input type="text" name="status" value="${treatmentEventDto.cancelReason}" path="cancelReason" class="form-control"/></td>
					</tr>
				</table>
				<div>
					<form:input path="id" type="hidden" value="${treatmentEventDto.id}" />
				</div>
				<form:button type="submit" class="btn btn-info mt-1">Save status</form:button>
				<button type="button" onclick='window.location.href="treatmentEvent"' class="btn btn-info mt-1">Back</button>
			</form:form>
		</div>
		
		<%@include file="../fragment/footer.html"%>
	</div>
</body>
</html>