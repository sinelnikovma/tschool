document.addEventListener("DOMContentLoaded", () => {

	document.addEventListener('click', function (el) {
		if (el.target && el.target.classList.contains('js_remove')) {
			var child = el.target.closest('.datetime');
			child.parentNode.removeChild(child);
		}
	});
})

function addTimeBlock(day){
	let selector = '.'+ day + '_times';
	let rootElement = document.querySelector(selector);
	let firstChild = rootElement.firstChild;
	
	selector = '.example_' + day+'_time';
	
	let element = document.querySelector(selector).cloneNode(true);
		
	element.classList.remove('example_' + day+'_time');
	element.classList.remove('none_display');
	let input = element.getElementsByTagName('input')[0];
	input.removeAttribute("disabled");
	
	rootElement.insertBefore(element, firstChild);
	
	console.log(selector);
}
