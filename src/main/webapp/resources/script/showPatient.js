var id = 0;
document.addEventListener("DOMContentLoaded", () => {

let tab = document.querySelector('#tab');

        tab.addEventListener('click', e=>{
        	
        	try{
        	let sell = e.target;
            let row = sell.parentElement;
            id = row.children[0].innerHTML;
            selectRow(row, "selected");
        	} catch (e) {
				console.log(e.message);
			}
        });
})

function editUser(){
	window.location.href="addPatient?id=" + id;
}

function dischargeUser(){
	if(id > 0){
	window.location.href="dischargePatient?id=" + id;
	}
}

// function set class "select" on active row and clear class "select" on unused
// row
function selectRow(row, classname){
	let table = row.parentElement;

	if(row.classList.contains(classname)){
		toggleClass(row, classname);	
		id = 0;
	} else{		
		let count = table.rows.length;		 
		 for (let i = 0; i < count; i++) {
			table.rows[i].classList.remove(classname);
		}
		toggleClass(row, classname);
	}
}

// function toggle class in transferred element
function toggleClass(row, classname){
	row.classList.toggle(classname);
}

