// function set class "select" on active row and clear class "select" on unused
// row
function selectRow(row, classname){
	let table = row.parentElement;

	if(row.classList.contains(classname)){
		toggleClass(row, classname);	
		id = 0;
	} else{		
		let count = table.rows.length;		 
		 for (let i = 0; i < count; i++) {
			table.rows[i].classList.remove(classname);
		}
		toggleClass(row, classname);
	}
}

// function toggle class in transferred element
function toggleClass(row, classname){
	row.classList.toggle(classname);
}
