var prescnum = 0;
var patientId = 0

document.addEventListener("DOMContentLoaded", ()=> {
	let prescriptiontable = document.querySelector('#prescription_table');
	
	prescriptiontable.addEventListener('click', e=>{
		try{
		let sell = e.target;
		let row = sell.parentElement;
		prescnum = row.children[0].innerHTML;
		selectRow(row, "selected");
		console.log(prescnum);
		} catch (e) {
			console.log(e.message);
		}
	});
	
	let patientTable = document.querySelector("#patient_table");
	
	patientTable.addEventListener('click', e=> {
		try{
		let sell = e.target;
		let row = sell.parentElement;
		patientId = row.children[0].innerHTML;
		selectRow(row, "selected");
		window.location.href="showPrescriptionList?id=" + patientId;
		} catch (e) {
			console.log(e.message);
		}
	});
}, false)

function editPrescription(){
	window.location.href="addPrescription?prescnum=" + prescnum;
}

function cancelPrescription(){
	if(prescnum > 0){
	window.location.href="showPrescriptionList?id=" + patientId + "&prescnum=" +  prescnum + "&action=delete";
//		fetch("../addPrescription?prescnum=" + prescnum).then()
	}
}

//function set class "select" on active row and clear class "select" on unused
//row
function selectRow(row, classname){
	let table = row.parentElement;

	if(row.classList.contains(classname)){
		toggleClass(row, classname);	
		patientId = 0;
		prescnum = 0;
	} else{		
		let count = table.rows.length;		 
		 for (let i = 0; i < count; i++) {
			table.rows[i].classList.remove(classname);
		}
		toggleClass(row, classname);
	}
}

//function toggle class in transferred element
function toggleClass(row, classname){
	row.classList.toggle(classname);
}